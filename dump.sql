# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 127.0.0.1 (MySQL 5.5.5-10.1.31-MariaDB)
# Схема: suite7102
# Время создания: 2018-03-26 14:14:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_name_del` (`name`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_audit`;

CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_accounts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_bugs`;

CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_cases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_cases`;

CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_contacts`;

CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_cstm`;

CREATE TABLE `accounts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы accounts_opportunities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts_opportunities`;

CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы acl_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_actions`;

CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_actions` WRITE;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;

INSERT INTO `acl_actions` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `category`, `acltype`, `aclaccess`, `deleted`)
VALUES
	('1075016e-0302-240d-0bd3-5ab8f7291b58','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_Quotes','module',90,0),
	('10800ab8-a022-1543-9a7c-5ab8f607545c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Prospects','module',90,0),
	('11355354-eb63-462a-4f05-5ab8f6296076','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Tasks','module',90,0),
	('11360fd3-2298-6282-d55a-5ab8f6c06cfb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Accounts','module',90,0),
	('1175de08-e878-f50f-68b1-5ab8f6e6e533','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AM_TaskTemplates','module',90,0),
	('12c65095-7569-e545-bede-5ab8f74a6f67','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_Product_Categories','module',90,0),
	('13ac54c6-a92b-36a7-1bf4-5ab8f7da5c0d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','OutboundEmailAccounts','module',90,0),
	('147c3150-1d68-fbf3-81d9-5ab8f730433a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','SurveyQuestionResponses','module',90,0),
	('15151c01-ddb2-cefc-f0f8-5ab8f6d2ba59','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','FP_events','module',90,0),
	('153c4d24-43a0-d075-89b9-5ab8f61492a9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOP_Case_Updates','module',89,0),
	('160690a5-9c33-93c8-adad-5ab8f614877a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AM_TaskTemplates','module',90,0),
	('163be06c-dbb1-25e8-d2c8-5ab8f6cb5a0d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Calls','module',89,0),
	('16de9eea-d498-5381-9852-5ab8f71b7d29','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_Product_Categories','module',90,0),
	('183be500-f50a-ee1d-70df-5ab8f7cba830','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','SurveyQuestionResponses','module',90,0),
	('189c9cc1-93dc-40a9-dac7-5ab8f7e3590a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','OutboundEmailAccounts','module',90,0),
	('19771afd-3fea-5b22-bf04-5ab8f70e3a6f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','jjwg_Areas','module',89,0),
	('19ac1970-bcfc-6612-4da0-5ab8f66b6fc3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Tasks','module',90,0),
	('1a7dc6d9-db01-066e-c09c-5ab8f6079a4e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AM_TaskTemplates','module',90,0),
	('1b80fd74-7fc6-2497-3cac-5ab8f6a33857','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_Contracts','module',89,0),
	('1c0afcf0-d0dc-491c-712d-5ab8f7ab755c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','SurveyQuestionResponses','module',90,0),
	('1c66ebd4-1824-8508-6a49-5ab8f6aa199c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOP_Case_Updates','module',90,0),
	('1c9cbb02-b758-55ce-e00b-5ab8f661029a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','ProjectTask','module',89,0),
	('1cc7ad7a-306b-6e69-b35e-5ab8f7e6e7f3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','jjwg_Areas','module',90,0),
	('1e22f11e-2242-1556-8722-5ab8f66cf380','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Calls','module',90,0),
	('1f1eac10-10ba-c1a1-301f-5ab8f6c78032','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AM_TaskTemplates','module',90,0),
	('1f8c6a87-b219-e219-1fc8-5ab8f68709a2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','EmailMarketing','module',89,0),
	('1f95fb6b-4492-7d6b-1853-5ab8f7a001b2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOW_WorkFlow','module',89,0),
	('1fc38fdd-a78a-a985-6529-5ab8f6505e2f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_Contracts','module',90,0),
	('205ead76-bbc4-a79a-1125-5ab8f656a8f4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Tasks','module',90,0),
	('205ee271-4131-4184-7af2-5ab8f78ca04e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','jjwg_Areas','module',90,0),
	('20a15345-d09a-5350-d4ac-5ab8f7c2d391','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_Product_Categories','module',90,0),
	('2115a317-ac04-95a2-54bd-5ab8f68a45ff','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','ProjectTask','module',90,0),
	('212e4aa4-3141-9605-2c89-5ab8f6508ac8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Bugs','module',89,0),
	('21712e42-0fb5-a32d-1494-5ab8f7fd56d2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','SurveyQuestionResponses','module',90,0),
	('2290194b-bf6c-c21d-027a-5ab8f66369e0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Opportunities','module',89,0),
	('229bc8d9-0d43-fe1b-ae27-5ab8f607c461','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','EmailMarketing','module',90,0),
	('22a9788e-7981-d331-fd3c-5ab8f69e7a05','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOP_Case_Updates','module',90,0),
	('2305ee6c-60c9-d241-1b44-5ab8f65ee6ce','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AM_TaskTemplates','module',90,0),
	('230acfcc-e9ff-e13e-cfd2-5ab8f707fa04','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOW_WorkFlow','module',90,0),
	('246ec88b-69e0-4554-6307-5ab8f6dafe3e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_Contracts','module',90,0),
	('24d4a4bc-85e6-1801-e8a4-5ab8f683d5f2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Bugs','module',90,0),
	('24ee6530-dd62-95a9-f398-5ab8f72c93d8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','SurveyQuestionResponses','module',90,0),
	('2508c423-85e9-e55b-4fea-5ab8f61b52d6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Tasks','module',90,0),
	('250f065a-37e9-3d31-f5ba-5ab8f719bd3a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','jjwg_Markers','module',90,0),
	('25c47a33-3673-393c-f0f2-5ab8f70b150e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','jjwg_Areas','module',90,0),
	('25fd3be4-317c-7334-5c81-5ab8f6f0bc90','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','EmailMarketing','module',90,0),
	('26c32615-56aa-84c4-96cf-5ab8f7faf56d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOW_WorkFlow','module',90,0),
	('26c3554c-2d1c-88dd-8433-5ab8f74bb897','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_Product_Categories','module',90,0),
	('26fc2be2-0381-853a-26b4-5ab8f6289e35','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Opportunities','module',90,0),
	('2873f909-c126-f85b-2c6e-5ab8f6c07b8a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOP_Case_Updates','module',90,0),
	('28be6e99-2bde-83d7-1c57-5ab8f6c60e60','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','ProjectTask','module',90,0),
	('28cb5799-a4fb-4a59-2f37-5ab8f69a8a03','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Tasks','module',90,0),
	('2959f543-f3aa-00a1-eeea-5ab8f6cb759d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Bugs','module',90,0),
	('29fdae88-4510-75bb-d927-5ab8f6b747cd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','EmailMarketing','module',90,0),
	('2a748f63-a2d9-3bfb-af7e-5ab8f62f96d1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_Contracts','module',90,0),
	('2acdb602-0bc5-86d6-04b7-5ab8f6ec7f3e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Opportunities','module',90,0),
	('2acfe1eb-d276-101a-ea2b-5ab8f7fe7700','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOW_WorkFlow','module',90,0),
	('2b0d0aa6-8087-2504-41a5-5ab8f79fe76e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_Product_Categories','module',89,0),
	('2c1948ad-7c08-33ea-5e15-5ab8f6750f01','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','FP_Event_Locations','module',89,0),
	('2c2a7167-79f5-0f82-f324-5ab8f60d866d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Spots','module',89,0),
	('2ce258f0-e199-24a4-d9f5-5ab8f61210ad','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Tasks','module',90,0),
	('2d395d8c-9f37-5e40-170f-5ab8f75b5219','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','jjwg_Areas','module',90,0),
	('2d6b4be0-6b6c-ffe5-a5f8-5ab8f602769f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','EmailMarketing','module',90,0),
	('2de13e25-d9e3-5aed-11ac-5ab8f6bf70e4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOP_Case_Updates','module',90,0),
	('2de85090-c393-e429-9496-5ab8f686b675','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Bugs','module',90,0),
	('2dec4cba-47ae-01b1-2a50-5ab8f7fdd892','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','TemplateSectionLine','module',89,0),
	('2e0ee0d6-91f2-1616-36db-5ab8f6c85f7c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','ProjectTask','module',90,0),
	('2f57f4c5-d71c-694c-2f17-5ab8f69ed86f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Opportunities','module',90,0),
	('2fc88066-af82-767b-816e-5ab8f78ea40d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOW_WorkFlow','module',90,0),
	('2fcf1fd9-b8c6-2328-eb74-5ab8f6039b87','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','FP_Event_Locations','module',90,0),
	('307ca687-530e-077e-7d7e-5ab8f6734ac4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_Contracts','module',90,0),
	('30955dcc-adea-8786-a5d7-5ab8f6f083e6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','EmailMarketing','module',90,0),
	('31fd7afa-bbd2-e629-fb65-5ab8f6308b02','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Calls','module',90,0),
	('32919375-e0f2-fc56-45f1-5ab8f6b56878','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOP_Case_Updates','module',90,0),
	('32ae99a0-be4a-d7cf-473e-5ab8f6b3725e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','ProjectTask','module',90,0),
	('337260e8-dfd8-2b5a-fc6d-5ab8f64992b9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','FP_Event_Locations','module',90,0),
	('338d636e-e832-6d77-4fb2-5ab8f7ac37ef','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOW_WorkFlow','module',90,0),
	('33f29a42-3de2-cade-47e5-5ab8f6387c11','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Opportunities','module',90,0),
	('34068b17-918b-cee3-c68f-5ab8f66e28bb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Spots','module',90,0),
	('341a7176-c9ae-2e85-7a65-5ab8f690e28a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','EmailMarketing','module',90,0),
	('346d22ac-8f93-afd8-9748-5ab8f7f56ae4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','TemplateSectionLine','module',90,0),
	('34995e2f-168d-7ce1-4d20-5ab8f6f9e21b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Bugs','module',90,0),
	('36236727-6e57-2c92-68da-5ab8f69bf7c0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_Contracts','module',90,0),
	('364b6e36-85b7-2c40-0e46-5ab8f7d2b64a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','jjwg_Areas','module',90,0),
	('36893960-7792-16a0-6f3f-5ab8f683389a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','ProjectTask','module',90,0),
	('36a409df-bcc9-47ee-858e-5ab8f779838d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','OutboundEmailAccounts','module',90,0),
	('36ce0698-51c9-f17f-1eca-5ab8f759f7d5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','SurveyQuestionResponses','module',89,0),
	('36e0f3e4-0c65-3892-5553-5ab8f6445611','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','FP_Event_Locations','module',90,0),
	('374137bc-3984-7ebf-1ccc-5ab8f7284539','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOW_WorkFlow','module',90,0),
	('37557aa1-1300-ba4a-7abe-5ab8f6dbdfeb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Opportunities','module',90,0),
	('3799443d-7fe2-f1fd-2e52-5ab8f6bd9a31','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','EmailMarketing','module',90,0),
	('38b6f9ee-04d4-922d-2e0e-5ab8f7e8d380','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','TemplateSectionLine','module',90,0),
	('39f0e632-cb8f-7340-3540-5ab8f64b189c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Spots','module',90,0),
	('3a4c837a-f1e1-46c9-946e-5ab8f681609c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Bugs','module',90,0),
	('3a71262d-f992-8c6b-9d88-5ab8f696c78f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','FP_Event_Locations','module',90,0),
	('3a860bed-b338-b790-9bb7-5ab8f6d61093','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOP_Case_Updates','module',90,0),
	('3aaaaf01-5244-317f-cdb7-5ab8f76d5c54','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOW_WorkFlow','module',90,0),
	('3ac45b2d-853a-2c61-4a4e-5ab8f697f78b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','ProjectTask','module',90,0),
	('3b0c846e-ed11-8067-db79-5ab8f66ecb1a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Calls','module',90,0),
	('3b493c69-386e-143e-69d2-5ab8f65abbd8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_Contracts','module',90,0),
	('3b556a2d-54f6-cd5a-e2d8-5ab8f725bb9d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','jjwg_Areas','module',90,0),
	('3b9bdc86-1cd7-8f1a-addc-5ab8f6eed3bd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Opportunities','module',90,0),
	('3d6ba15c-6d74-f13b-a4aa-5ab8f7498d62','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','TemplateSectionLine','module',90,0),
	('3e116316-ab7a-fbe7-ef97-5ab8f607ab7a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Spots','module',90,0),
	('3e3c5549-6ebd-6ea5-e81f-5ab8f69e4625','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','FP_Event_Locations','module',90,0),
	('3e4ec99f-ebf1-b63c-57d2-5ab8f67f9eff','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Bugs','module',90,0),
	('3e969dd6-b148-0725-d689-5ab8f686e96c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Opportunities','module',90,0),
	('3f26430a-a9a5-658c-87d2-5ab8f61bb6f5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','ProjectTask','module',90,0),
	('3f2e0c35-1f41-aa5e-61cd-5ab8f6836c84','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','FP_events','module',90,0),
	('3f7cbe6b-d0ec-0c12-fed9-5ab8f700ed6a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','jjwg_Areas','module',90,0),
	('3fee75d6-7af9-3fc8-4e5e-5ab8f7cee0ab','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','SurveyQuestions','module',89,0),
	('401ebef9-5350-fb0b-cc17-5ab8f615fbce','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Calls','module',90,0),
	('40d0a395-22f8-ffdd-0e99-5ab8f6553061','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOP_Case_Updates','module',90,0),
	('413539a1-6efa-8649-17a0-5ab8f7b3ad91','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','TemplateSectionLine','module',90,0),
	('423b88c0-27d0-a7b5-c3ed-5ab8f65d5ae7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Bugs','module',90,0),
	('425a9c10-2f47-9fc8-b8a3-5ab8f658d87d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','FP_Event_Locations','module',90,0),
	('428ac03c-7014-748a-9e8e-5ab8f69fe9d2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Spots','module',90,0),
	('442b8f8e-7495-d5e7-29bf-5ab8f6211bab','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Calls','module',90,0),
	('4449f1e3-bee6-ce50-c2ce-5ab8f7d9ca77','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','SurveyQuestions','module',90,0),
	('44f8c1f1-604f-ba60-590a-5ab8f616734b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOK_Knowledge_Base_Categories','module',89,0),
	('4522e0a2-1a50-c0bc-3dfe-5ab8f6031ab2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_Contracts','module',90,0),
	('459f0f9b-b98e-379a-cd9b-5ab8f72e976c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','TemplateSectionLine','module',90,0),
	('47efc041-3e83-ddec-6f51-5ab8f76512e4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','SurveyQuestions','module',90,0),
	('48ca161d-3524-1f8c-1807-5ab8f7df2d39','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','TemplateSectionLine','module',90,0),
	('4943a1a6-af9d-430a-82d2-5ab8f6a3fdd2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Spots','module',90,0),
	('4a8f2687-63a8-0ecf-bb1b-5ab8f60cde27','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','FP_Event_Locations','module',90,0),
	('4acd7d97-47b8-9e67-0357-5ab8f6975855','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOK_Knowledge_Base_Categories','module',90,0),
	('4c1eb17c-cf85-b3cb-45f3-5ab8f7f5fad7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','SurveyQuestions','module',90,0),
	('4c913221-22fa-fe6f-db2e-5ab8f64554a3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Calls','module',90,0),
	('4cb8f06a-df34-40af-4808-5ab8f7d7d7ae','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','TemplateSectionLine','module',90,0),
	('4cbe0389-23ef-3039-c600-5ab8f699567f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Spots','module',90,0),
	('4cfd0019-255e-3486-2326-5ab8f6e17287','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Prospects','module',90,0),
	('4f970aa9-0b74-fa84-12ca-5ab8f67a7fae','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOK_Knowledge_Base_Categories','module',90,0),
	('5094496b-d0bf-8663-7a04-5ab8f7f7c4aa','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','SurveyQuestions','module',90,0),
	('50a7fcd2-c4d7-36ba-00bc-5ab8f66662ba','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Calls','module',90,0),
	('50b00a97-a363-2de4-7090-5ab8f66fd3ce','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Spots','module',90,0),
	('52026c89-6e31-a02b-33d0-5ab8f7e0eb86','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOW_Processed','module',89,0),
	('532e2005-708a-8185-0629-5ab8f7da3505','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','jjwg_Address_Cache','module',89,0),
	('54e379ec-a1d7-0c71-16e1-5ab8f707de35','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOW_Processed','module',90,0),
	('54ee5dab-e025-05f7-787e-5ab8f6630c44','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOK_Knowledge_Base_Categories','module',90,0),
	('5557a1a1-6175-a332-ea25-5ab8f7bf8bc8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','SurveyQuestions','module',90,0),
	('55d3d83b-6397-c40c-f9a2-5ab8f75372c7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_Products','module',89,0),
	('56862223-7166-de7a-ece7-5ab8f762b10f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','jjwg_Address_Cache','module',90,0),
	('57ed087f-7c8e-4f3c-65e1-5ab8f6cf9cca','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOR_Scheduled_Reports','module',90,0),
	('594c9759-eea7-d74e-d51a-5ab8f7cacc03','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','jjwg_Address_Cache','module',90,0),
	('595c10a6-47c9-31f6-9051-5ab8f66d1670','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOR_Reports','module',89,0),
	('59a175f9-d8e9-ed91-c813-5ab8f65eb204','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOK_Knowledge_Base_Categories','module',90,0),
	('59bcdfee-9798-abaf-4947-5ab8f7ef6001','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','SurveyQuestions','module',90,0),
	('59e571b4-c1bc-72d7-ebb1-5ab8f7e0e0aa','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOW_Processed','module',90,0),
	('5d349099-158d-d335-822a-5ab8f71ece11','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','jjwg_Address_Cache','module',90,0),
	('5eb2e3c2-4c9b-62d0-1fc7-5ab8f797feed','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_Products','module',90,0),
	('5ec054e4-a108-01ac-1985-5ab8f739bb0e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOW_Processed','module',90,0),
	('5edbc71a-be45-023a-7fc7-5ab8f6028f83','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Campaigns','module',89,0),
	('603fd551-77ae-6771-94c5-5ab8f75c5887','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','SurveyQuestions','module',90,0),
	('60a421db-7361-4e89-23c2-5ab8f68fe6f9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOK_Knowledge_Base_Categories','module',90,0),
	('617da242-0c1c-7834-11c8-5ab8f699210e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Leads','module',89,0),
	('61966fe0-c560-7554-3a81-5ab8f7445ef9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','jjwg_Address_Cache','module',90,0),
	('61f91b56-5c0c-4ca8-d817-5ab8f6d6abd6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOR_Reports','module',90,0),
	('62b9dd0a-0274-ce2c-0143-5ab8f7e1ec4e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_Products','module',90,0),
	('64290b60-ed29-0b5a-338b-5ab8f63a449d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AM_TaskTemplates','module',89,0),
	('64307347-e8b3-40b4-5bb6-5ab8f613055b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOK_Knowledge_Base_Categories','module',90,0),
	('64368bed-c1dd-2fb3-080a-5ab8f6afdeab','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOD_IndexEvent','module',89,0),
	('6546936d-c15b-bba6-f775-5ab8f7094058','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','jjwg_Address_Cache','module',90,0),
	('65a04972-f075-1aae-fc26-5ab8f69408a9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOR_Reports','module',90,0),
	('66dad9b3-d595-54b1-a8e5-5ab8f7274dac','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOW_Processed','module',90,0),
	('6724f700-c3c0-dc8b-4743-5ab8f6221222','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Emails','module',89,0),
	('6785c9c2-6172-3933-2c3f-5ab8f7112781','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_Products','module',90,0),
	('67cba13b-cfc9-a0bc-2b1a-5ab8f6538262','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Campaigns','module',90,0),
	('68149c1f-4829-ab9c-b93f-5ab8f6cfba63','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOD_IndexEvent','module',90,0),
	('684a6c1a-7152-e971-5cd2-5ab8f6474390','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOK_Knowledge_Base_Categories','module',90,0),
	('684e3a70-1563-39c1-0cd7-5ab8f688b562','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','EmailTemplates','module',89,0),
	('689a1271-53e0-a1a5-69dc-5ab8f797d9ec','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','jjwg_Address_Cache','module',90,0),
	('696314bc-7631-62ce-f22f-5ab8f6b6db53','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_Invoices','module',89,0),
	('69d04074-8fa7-465b-ea64-5ab8f62c638c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOR_Reports','module',90,0),
	('6babff47-cad4-c3b2-ff69-5ab8f60074a9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOD_IndexEvent','module',90,0),
	('6baef4bd-1504-668c-1e78-5ab8f6946fd2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Users','module',89,0),
	('6bd1b4ad-7a9c-e5d5-8b5e-5ab8f7b69f2f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_Products','module',90,0),
	('6c079ed0-23ec-e42d-a445-5ab8f7372c02','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOW_Processed','module',90,0),
	('6c276d3d-2d27-f90f-0927-5ab8f6f2619f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Emails','module',90,0),
	('6c2ff9c2-08d8-54ce-2ad9-5ab8f6f5209d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Leads','module',90,0),
	('6c812c90-beb1-0cbd-8968-5ab8f7634b2f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','jjwg_Address_Cache','module',90,0),
	('6d9ef871-ac22-1f8a-0fbd-5ab8f6336fc4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOR_Reports','module',90,0),
	('6dba4174-1180-4f8e-621d-5ab8f68fc000','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_Invoices','module',90,0),
	('6e1406ae-b11b-d6c1-7a91-5ab8f7d85ab8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_Product_Categories','module',90,0),
	('6e92999c-018e-7f15-78c1-5ab8f6455406','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','EmailTemplates','module',90,0),
	('6f5a7bc0-dbb7-84f7-debf-5ab8f68c6c8d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOD_IndexEvent','module',90,0),
	('6fdb5985-e425-66b3-d78c-5ab8f75e4745','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOW_Processed','module',90,0),
	('6ff1f63a-5f38-34d3-9895-5ab8f6bef9b5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Users','module',90,0),
	('707a881b-c2b8-0546-9e0e-5ab8f67344f0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOR_Reports','module',90,0),
	('70d6c331-e043-30e9-cddf-5ab8f62e7c2f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Leads','module',90,0),
	('70eff018-5732-997a-2c04-5ab8f666f6f7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Campaigns','module',90,0),
	('711ab522-b009-e2c2-5816-5ab8f7597998','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_Products','module',90,0),
	('712c6728-7b53-92c7-72a1-5ab8f669cb48','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_Invoices','module',90,0),
	('71f7091e-5a91-deaf-47f0-5ab8f6c1733c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Emails','module',90,0),
	('736e5473-f458-e0c6-89b6-5ab8f6f3724e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOD_IndexEvent','module',90,0),
	('73e3e9c3-c71f-b8aa-b30d-5ab8f7dcdcf9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOW_Processed','module',90,0),
	('74a54a84-7593-45ee-e849-5ab8f62f320d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOR_Reports','module',90,0),
	('74c9100c-4e11-f4b7-082f-5ab8f64ba807','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Alerts','module',89,0),
	('75752a2a-baf1-2d02-41b1-5ab8f6880d32','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Campaigns','module',90,0),
	('7605efd3-10dc-2ca2-b7a7-5ab8f6e4afe6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Leads','module',90,0),
	('7701fad4-aa29-770c-c00d-5ab8f6baef2d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOD_IndexEvent','module',90,0),
	('77119f30-8427-aade-e1a3-5ab8f72bddba','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_Products','module',90,0),
	('77a5435d-4acb-9395-af3d-5ab8f644b8fd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Emails','module',90,0),
	('77ddeaa9-e43d-2260-e649-5ab8f6f109b5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Users','module',90,0),
	('77eecead-181a-b51f-3e6b-5ab8f7911b4a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','SurveyQuestionOptions','module',89,0),
	('7863c0b4-bc34-4d0c-7576-5ab8f6224a4a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Campaigns','module',90,0),
	('786d23b5-1855-e41f-4f79-5ab8f6f754e6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOR_Reports','module',90,0),
	('78837baf-9b24-d730-2d9c-5ab8f726aec0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','SurveyResponses','module',89,0),
	('789fac31-888e-31ba-6397-5ab8f6d68157','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Alerts','module',90,0),
	('7981cb2b-f4e3-09c3-00a3-5ab8f6c57d05','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOD_IndexEvent','module',90,0),
	('79b886cc-5c62-9249-1b24-5ab8f6058ac6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Leads','module',90,0),
	('7b19242a-6351-b388-038b-5ab8f671f5d4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','EmailTemplates','module',90,0),
	('7b393b2b-ad09-9de2-d7d6-5ab8f7bc44a2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_Products','module',90,0),
	('7bad2a49-70e8-38ce-aa09-5ab8f73eb699','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','SurveyQuestionOptions','module',90,0),
	('7bebcece-af6c-3f65-2913-5ab8f68a9f35','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Users','module',90,0),
	('7c06d3d3-bbf8-eba6-d48b-5ab8f6231eec','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','EAPM','module',89,0),
	('7c430dff-efe9-7a03-add6-5ab8f7726899','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','SurveyQuestionResponses','module',90,0),
	('7c67877c-fde1-20dd-cb3c-5ab8f64fb345','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Campaigns','module',90,0),
	('7c8724d4-c37a-acc6-7e0d-5ab8f6af8e19','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOD_IndexEvent','module',90,0),
	('7cac9126-38cf-667b-d4ed-5ab8f62ac5a4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Alerts','module',90,0),
	('7d17e8c9-039d-9250-d64a-5ab8f740feae','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','SurveyResponses','module',90,0),
	('7f2cca26-6b62-0984-2be6-5ab8f65797b6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOK_KnowledgeBase','module',89,0),
	('7f6acb17-1d55-7153-5f9d-5ab8f78216c9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','SurveyQuestionOptions','module',90,0),
	('800ef373-8eab-8377-b743-5ab8f686f098','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Campaigns','module',90,0),
	('8028149e-61c9-c12a-be13-5ab8f6ac152c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Users','module',90,0),
	('8064caf5-d2cd-6cc9-837e-5ab8f6a080a0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','EAPM','module',90,0),
	('82662a72-be60-c926-517e-5ab8f6862d95','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Alerts','module',90,0),
	('82ea732f-5bee-f8ee-1f73-5ab8f7acbf28','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Calls_Reschedule','module',89,0),
	('83407ba0-0e81-010c-b0bf-5ab8f69e32a1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Leads','module',90,0),
	('834a8795-5e6e-be28-dec1-5ab8f6178d35','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','EmailTemplates','module',90,0),
	('837e1844-a527-ccb8-9d05-5ab8f71d512f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','SurveyResponses','module',90,0),
	('8398c16f-9799-fdf9-ee24-5ab8f688709a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Emails','module',90,0),
	('83a93344-b22c-afc2-8611-5ab8f6140d17','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Campaigns','module',90,0),
	('840c3fd7-3d02-b79f-9167-5ab8f7ae9f40','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','SurveyQuestionOptions','module',90,0),
	('8433dea2-df4c-d45e-a37d-5ab8f676d1a0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOK_KnowledgeBase','module',90,0),
	('8460e0a0-0366-177a-582d-5ab8f60947ff','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','EAPM','module',90,0),
	('846a360d-3d9c-8755-2500-5ab8f60b4d7e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Users','module',90,0),
	('847905db-f069-cdff-2469-5ab8f6fe77f3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Tasks','module',89,0),
	('858652e0-255a-2d11-53e7-5ab8f6dd01a7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Alerts','module',90,0),
	('868d1b57-c333-9163-ea79-5ab8f6a2aa4a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','EmailTemplates','module',90,0),
	('878d865d-6ba1-d241-718d-5ab8f6dbfe8c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Users','module',90,0),
	('87954516-8044-5d3f-3b1e-5ab8f72ace56','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Calls_Reschedule','module',90,0),
	('87a032b5-9a60-f64f-c882-5ab8f73d929e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','SurveyResponses','module',90,0),
	('88220f46-f7cb-8103-3b7e-5ab8f61328e6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOK_KnowledgeBase','module',90,0),
	('882775a0-f055-5583-385a-5ab8f60165d6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_Invoices','module',90,0),
	('8831831c-fcea-5735-71e5-5ab8f6744772','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Leads','module',90,0),
	('887c2a70-fdce-24d7-29c9-5ab8f6a55f50','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','EAPM','module',90,0),
	('88b67a32-b941-4be7-7d97-5ab8f726e67f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','SurveyQuestionOptions','module',90,0),
	('89ac2859-f66b-5fc3-7b54-5ab8f6aae65e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Alerts','module',90,0),
	('89feea75-04c6-5d43-49cc-5ab8f64acfd9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','EmailTemplates','module',90,0),
	('8a3d3a0f-d29f-2c87-ea49-5ab8f62e0924','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Contacts','module',89,0),
	('8ad2e33b-b12e-0bdf-2b2a-5ab8f69571a1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Emails','module',90,0),
	('8ba27750-6be7-771b-0caa-5ab8f79a6f98','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','SurveyResponses','module',90,0),
	('8be7d20d-4f83-47d1-b97f-5ab8f7189892','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Calls_Reschedule','module',90,0),
	('8c1c1a66-cc76-9b39-c8e9-5ab8f64af2b5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Leads','module',90,0),
	('8c7af6d3-3e67-7bcd-e6ad-5ab8f6ecf05a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Users','module',90,0),
	('8ced3ce0-5693-de52-a8b9-5ab8f652a052','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','EAPM','module',90,0),
	('8d0b5035-bc19-a1da-494a-5ab8f77fb2f0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','SurveyQuestionOptions','module',90,0),
	('8d2c20a3-0507-db18-4826-5ab8f606fd8c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Alerts','module',90,0),
	('8d920685-09c1-9c50-f906-5ab8f67e95c6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Contacts','module',90,0),
	('8df391ba-e28c-285b-39b7-5ab8f6e8f31a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOK_KnowledgeBase','module',90,0),
	('8ea1763a-4998-1e87-fe6c-5ab8f6f3b663','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','EmailTemplates','module',90,0),
	('8f91caed-2e4b-5526-03f4-5ab8f64b7c04','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_Invoices','module',90,0),
	('8fbfe542-2d4e-3c52-cbab-5ab8f782e8c7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Calls_Reschedule','module',90,0),
	('9051e168-5d59-b99b-fc20-5ab8f6cbec9a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Contacts','module',90,0),
	('90e0b8a7-4278-3614-c096-5ab8f7bbd86a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','SurveyResponses','module',90,0),
	('9145a6b4-c52c-3f94-7280-5ab8f7183360','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','SurveyQuestionOptions','module',90,0),
	('917870c3-aba2-cf2a-594a-5ab8f62e713e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Alerts','module',90,0),
	('9244d0d6-2c3c-0211-ed5f-5ab8f63e7c51','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','EmailTemplates','module',90,0),
	('9308e66c-677a-3620-d652-5ab8f67d29f9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_Invoices','module',90,0),
	('938f00f9-f3e1-d7d4-0917-5ab8f6212048','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Prospects','module',90,0),
	('93b4dc0c-7fd9-1e01-29d8-5ab8f7465d21','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Calls_Reschedule','module',90,0),
	('944b023e-3463-a716-3bf1-5ab8f622cdfe','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Contacts','module',90,0),
	('9457021c-2dfb-4891-1aa1-5ab8f6511bc6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','FP_events','module',90,0),
	('946a9c1d-17a0-1ed1-c428-5ab8f7c277fd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','jjwg_Maps','module',89,0),
	('94f88eed-6b5b-d6a0-8be1-5ab8f63fd3a5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOK_KnowledgeBase','module',90,0),
	('959b675f-9b98-b567-ffd1-5ab8f620557b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_Invoices','module',90,0),
	('95fb16b7-5718-4c55-f19d-5ab8f794edf9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','SurveyQuestionOptions','module',90,0),
	('973e0476-05c2-845c-dbff-5ab8f780b24b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','OutboundEmailAccounts','module',90,0),
	('97e1d21c-db01-9ee3-64dd-5ab8f7f526d9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Calls_Reschedule','module',90,0),
	('984f5147-f841-c9cd-bbb2-5ab8f75f7e77','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','SurveyResponses','module',90,0),
	('98dea76a-5463-6b68-2cca-5ab8f741dd57','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','jjwg_Maps','module',90,0),
	('99f57d59-79f1-a72a-495a-5ab8f652e210','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOD_Index','module',89,0),
	('9b731c58-fa54-837b-7606-5ab8f6070407','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Contacts','module',90,0),
	('9c21ed44-ee19-b668-4df7-5ab8f62a5fe2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Emails','module',90,0),
	('9cb14188-6ca5-30e8-1743-5ab8f7d1cf12','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','SurveyResponses','module',90,0),
	('9cd5e6d8-426d-7757-5076-5ab8f73f77cc','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Calls_Reschedule','module',90,0),
	('9d5c94d6-593d-b10f-a435-5ab8f7895b28','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','jjwg_Maps','module',90,0),
	('9d68ee1b-d8ba-b362-2c1a-5ab8f614a105','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','EAPM','module',90,0),
	('9ec63f54-6ee9-960b-7729-5ab8f64047e0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_Invoices','module',90,0),
	('9ef909a0-f985-5b85-3fae-5ab8f6226d8e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AM_TaskTemplates','module',90,0),
	('9f8f4f2d-bd1d-ab23-2994-5ab8f63f57d8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Notes','module',89,0),
	('9fd90a0f-da1f-7565-13c2-5ab8f6749ec2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Contacts','module',90,0),
	('a0b860d6-012d-575a-bcd1-5ab8f6076a5a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','EAPM','module',90,0),
	('a0dae69e-bee0-93c4-8443-5ab8f7b92b58','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Calls_Reschedule','module',90,0),
	('a2a6421e-3abe-5532-6f02-5ab8f6e9bc00','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Notes','module',90,0),
	('a2d28067-5d76-0127-e3ba-5ab8f6709bb6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOD_Index','module',90,0),
	('a3f7e4d8-277e-5a41-d3eb-5ab8f6d93c6d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Emails','module',90,0),
	('a4371af4-b656-88ff-31e1-5ab8f6f3dc31','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Contacts','module',90,0),
	('a48e3f78-b83b-efa5-8162-5ab8f764f58a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','jjwg_Maps','module',90,0),
	('a4cefde9-306c-915a-115b-5ab8f6565b32','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','EAPM','module',90,0),
	('a64e4fb2-83ec-fbc5-3039-5ab8f61606a5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Notes','module',90,0),
	('a6f3aa84-cffd-bfce-82ba-5ab8f63a514e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOD_Index','module',90,0),
	('a90d993c-0634-5754-bd0a-5ab8f60d921e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Contacts','module',90,0),
	('a9e60a2d-226f-ac7f-7524-5ab8f77a0e39','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_Product_Categories','module',90,0),
	('aac8d9be-b849-c7a5-8de7-5ab8f6ed2a8c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Notes','module',90,0),
	('aca80e36-2f09-09f3-5112-5ab8f6e0e1d7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOD_Index','module',90,0),
	('acd11cd8-9c16-4b45-0e6a-5ab8f6c40381','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOK_KnowledgeBase','module',90,0),
	('adb8d5d0-e995-e391-5563-5ab8f615bf62','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Notes','module',90,0),
	('addfda53-af20-e1e9-22bc-5ab8f6a5d719','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Documents','module',89,0),
	('aed626a6-2142-1100-f5b9-5ab8f758481f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','jjwg_Maps','module',90,0),
	('afe6816f-d88b-6714-849f-5ab8f62feff1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','ProspectLists','module',89,0),
	('b0cc5155-564e-1b68-304f-5ab8f6f089ad','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Project','module',89,0),
	('b143df19-d795-a935-dabd-5ab8f6748376','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOK_KnowledgeBase','module',90,0),
	('b15968f1-bad0-8209-9402-5ab8f60e8022','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOD_Index','module',90,0),
	('b1e4800b-4272-7a7d-e92c-5ab8f68e64c5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Notes','module',90,0),
	('b2e3717e-bbb1-acf1-7a32-5ab8f6c238b5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Documents','module',90,0),
	('b31f2c2f-ace2-294c-c571-5ab8f7be7867','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','jjwg_Maps','module',90,0),
	('b481f550-5f4a-a907-60cc-5ab8f6c01a3c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Cases','module',89,0),
	('b54a779b-f623-a7b0-ae33-5ab8f6079afe','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOK_KnowledgeBase','module',90,0),
	('b5517509-fe9f-d03d-492d-5ab8f692a5aa','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOD_Index','module',90,0),
	('b69f5a81-1811-4273-9bbe-5ab8f7ecef3b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','SecurityGroups','module',89,0),
	('b6daac5d-7853-3312-bc5a-5ab8f7f55afa','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','jjwg_Maps','module',90,0),
	('b6ed26ce-54b7-36b0-934c-5ab8f6982c72','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Notes','module',90,0),
	('b7529d4f-319a-f82d-31ae-5ab8f6af45d4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Documents','module',90,0),
	('b7cd1cbf-e3cb-3241-12ec-5ab8f6ea4e57','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Project','module',90,0),
	('b8ea8a21-f080-0f00-324b-5ab8f69d998d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOD_Index','module',90,0),
	('b93e93a2-8535-84ff-7937-5ab8f7c9a116','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','SecurityGroups','module',90,0),
	('b9f3e09d-05af-eef7-338e-5ab8f6ab3247','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Cases','module',90,0),
	('ba8ec671-4fd8-1564-7de8-5ab8f633bd71','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Documents','module',90,0),
	('bafa69db-93c1-81b4-2432-5ab8f6851202','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Notes','module',90,0),
	('bb16cbfa-ccd0-7788-d038-5ab8f7d362c2','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','jjwg_Maps','module',90,0),
	('bc84ec54-7c0a-ba27-ec01-5ab8f601c913','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOD_Index','module',90,0),
	('bce2f221-4aaa-f3df-0e42-5ab8f77b52ab','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','SecurityGroups','module',90,0),
	('bd059346-b3e8-c78f-2b18-5ab8f642f03e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','ProspectLists','module',90,0),
	('bd902b8c-56d0-2066-cc37-5ab8f6a7c83c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_PDF_Templates','module',89,0),
	('bdfbe9b0-1955-08ad-f030-5ab8f69fe9c1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Documents','module',90,0),
	('bedbdedf-603c-8b17-e6c6-5ab8f763217f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Surveys','module',89,0),
	('bf03c88f-e64e-fe6e-e5c4-5ab8f662eca4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Cases','module',90,0),
	('c02e8c90-6d98-5642-29b4-5ab8f6fd83af','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Meetings','module',89,0),
	('c05d0aa3-3528-1183-0e20-5ab8f6a1c578','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Project','module',90,0),
	('c107ea38-040e-080b-bb32-5ab8f62a23a8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_PDF_Templates','module',90,0),
	('c1775c5c-8687-14be-6177-5ab8f6c5fbce','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Documents','module',90,0),
	('c1d7e1f9-7b53-b69c-616d-5ab8f7a4b10c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','SecurityGroups','module',90,0),
	('c3473a57-54b5-536e-94b6-5ab8f60baa7f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','ProspectLists','module',90,0),
	('c350aeed-dd9a-29c9-bab3-5ab8f77b864a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Surveys','module',90,0),
	('c40a171e-0b93-244c-1c88-5ab8f605b644','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AM_ProjectTemplates','module',89,0),
	('c419b2b1-ac9b-1ead-e90c-5ab8f6089964','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Cases','module',90,0),
	('c47987a9-993f-356b-2a70-5ab8f61714ac','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_PDF_Templates','module',90,0),
	('c490c698-73f8-3d9f-d4fe-5ab8f6cd69ca','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Meetings','module',90,0),
	('c4d5b921-7929-8c04-aa77-5ab8f61093bc','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Documents','module',90,0),
	('c6f0ed15-3b88-db81-98db-5ab8f7d0a520','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Surveys','module',90,0),
	('c75b3e57-4aeb-88a1-e277-5ab8f77c5ddb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','SecurityGroups','module',90,0),
	('c7a214f0-4e45-962d-8a08-5ab8f6f759b7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AM_ProjectTemplates','module',90,0),
	('c7bcc207-e105-eb28-72f8-5ab8f64347bc','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_PDF_Templates','module',90,0),
	('c7d1d97d-db44-d809-e5a3-5ab8f699926e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','ProspectLists','module',90,0),
	('c8470293-bb74-d8f3-4f3d-5ab8f6a03e78','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Documents','module',90,0),
	('c8727357-c140-d2f0-5517-5ab8f67bacec','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Cases','module',90,0),
	('c8c92cbb-05e9-bd80-339c-5ab8f633d57d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Meetings','module',90,0),
	('cbf1c533-bb60-416a-909a-5ab8f76f859b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Surveys','module',90,0),
	('cc070d90-90a7-9e9f-3fe7-5ab8f6c5a1dd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AM_ProjectTemplates','module',90,0),
	('cc47861c-5847-0258-bfce-5ab8f62efc83','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','ProspectLists','module',90,0),
	('cc77feae-36d2-1be1-ca37-5ab8f7ff5bd3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','SecurityGroups','module',90,0),
	('cca8e396-378e-4e9e-5f5f-5ab8f6bb266f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_PDF_Templates','module',90,0),
	('cce7976f-d2e7-7279-dc12-5ab8f64a09ac','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Meetings','module',90,0),
	('cf59344d-2096-2e75-de18-5ab8f6e725c4','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AM_ProjectTemplates','module',90,0),
	('cf717c28-3332-eec2-2fbd-5ab8f6457440','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','ProspectLists','module',90,0),
	('cf7c3c9e-211f-530d-7ace-5ab8f6be1237','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Cases','module',90,0),
	('cfb4e81d-b013-eed9-8cc6-5ab8f7bb39bb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Surveys','module',90,0),
	('d0a255cb-5c48-14fa-ae34-5ab8f6a3c1ad','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_PDF_Templates','module',90,0),
	('d0ee6cba-dccd-3809-66dc-5ab8f69d0549','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOP_Case_Events','module',89,0),
	('d121605e-6afe-d80c-f52e-5ab8f79012e9','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','SecurityGroups','module',90,0),
	('d16b059c-edb5-aa24-e067-5ab8f602ec08','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Tasks','module',90,0),
	('d16b8274-c644-927c-38d7-5ab8f6cb09b8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Accounts','module',89,0),
	('d1b003bb-e2e2-1bc6-2099-5ab8f6d5fe99','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Project','module',90,0),
	('d25274a1-375a-7308-9f90-5ab8f61daf89','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','ProspectLists','module',90,0),
	('d38a574f-23fc-a8a0-18ee-5ab8f7c2677d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Surveys','module',90,0),
	('d40412e9-7575-a3d4-38b7-5ab8f69bcedf','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AM_ProjectTemplates','module',90,0),
	('d43ff484-ec5c-ac6f-a234-5ab8f6857f36','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Project','module',90,0),
	('d4ca6fd9-ad74-626a-84c7-5ab8f6871f41','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOR_Scheduled_Reports','module',89,0),
	('d4e3079d-5047-7c4a-0803-5ab8f685cc74','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Accounts','module',90,0),
	('d506328a-8945-1010-d260-5ab8f6250f75','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Meetings','module',90,0),
	('d53bcf23-26e1-c8fc-8610-5ab8f6d0c328','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Cases','module',90,0),
	('d5447fb8-2d33-c0d2-1487-5ab8f6c12819','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOP_Case_Events','module',90,0),
	('d5a8a6aa-387e-2e6f-1f83-5ab8f614a78d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','ProspectLists','module',90,0),
	('d5f5a484-acea-235f-a43d-5ab8f74f5d22','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','SecurityGroups','module',90,0),
	('d67c322c-430d-fa22-9e00-5ab8f622a7d5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_PDF_Templates','module',90,0),
	('d70df4ae-2c10-fff3-66d1-5ab8f74a3dc1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','jjwg_Markers','module',89,0),
	('d74db4a0-242f-3366-a119-5ab8f7fec97e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Surveys','module',90,0),
	('d7a3083f-a303-0d3e-f956-5ab8f6d75f8a','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Prospects','module',90,0),
	('d81a0376-bf80-616d-0321-5ab8f7afce31','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','SurveyQuestionResponses','module',90,0),
	('d82e95a8-6872-7246-6aed-5ab8f6e6de50','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AM_ProjectTemplates','module',90,0),
	('d8912861-eecf-a999-263b-5ab8f7ef128d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','AOS_Quotes','module',89,0),
	('d93eced2-7e17-75c5-6f73-5ab8f6033d24','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOR_Scheduled_Reports','module',90,0),
	('d968d4e7-bd17-dd12-e6e7-5ab8f6a83c19','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOP_Case_Events','module',90,0),
	('d98b097f-7873-a0b1-f5b9-5ab8f7b9562c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','OutboundEmailAccounts','module',90,0),
	('d98e8419-e7bc-f42e-4507-5ab8f6c26401','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Cases','module',90,0),
	('d9a60b81-bdfe-3cb9-617d-5ab8f643a329','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AM_TaskTemplates','module',90,0),
	('da135e18-6781-be98-c23a-5ab8f67b9c7c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Project','module',90,0),
	('dbad59f9-de8c-8eda-29e7-5ab8f656fdbd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Meetings','module',90,0),
	('dcf39a15-10f7-ba8d-1768-5ab8f6b542d5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Accounts','module',90,0),
	('dd0fda26-8dad-7d49-57cb-5ab8f7d09f94','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','jjwg_Markers','module',90,0),
	('ddd3746c-73a5-2cbf-7268-5ab8f610432b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOS_PDF_Templates','module',90,0),
	('de318fa6-eb2b-17b6-26f6-5ab8f7c2f2b0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','AOS_Quotes','module',90,0),
	('de5adf9f-a694-0abf-064d-5ab8f63b5df6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOR_Scheduled_Reports','module',90,0),
	('dfb22760-9467-d7fe-d942-5ab8f6afb340','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AM_ProjectTemplates','module',90,0),
	('e020eb6f-de54-f7a3-4511-5ab8f72833db','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Surveys','module',90,0),
	('e0bb76a5-74ee-088a-ce07-5ab8f69842ca','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOP_Case_Events','module',90,0),
	('e0e1ef0b-a874-ea9a-05d0-5ab8f6611835','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Accounts','module',90,0),
	('e136a55a-9de8-b0be-12d7-5ab8f6926568','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Project','module',90,0),
	('e14f5729-5177-d8e0-0a98-5ab8f731a328','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','jjwg_Markers','module',90,0),
	('e1edc327-6c12-b7af-6b34-5ab8f7d6abb0','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','AOS_Quotes','module',90,0),
	('e1f06d98-a723-4b5d-dc9c-5ab8f6226448','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOR_Scheduled_Reports','module',90,0),
	('e34e6628-ba38-656d-0d4d-5ab8f612ecaf','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Meetings','module',90,0),
	('e4a22b19-cf0d-943d-6c5a-5ab8f6390e7e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AM_ProjectTemplates','module',90,0),
	('e56cdd7c-1d9d-8d9d-cc26-5ab8f7b4db42','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','jjwg_Markers','module',90,0),
	('e5812743-85e3-1f8a-2bd0-5ab8f6d6ec18','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','Accounts','module',90,0),
	('e58709d2-a899-1156-110c-5ab8f65f306d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOP_Case_Events','module',90,0),
	('e5f012a6-70e3-0dce-fd00-5ab8f7a51bc1','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_Quotes','module',90,0),
	('e614d7d7-4d1d-baee-2c24-5ab8f6086f22','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','FP_events','module',89,0),
	('e624114c-e69a-10b2-c4a2-5ab8f6ea562c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOR_Scheduled_Reports','module',90,0),
	('e6eca878-d93e-3fea-9631-5ab8f6f6ea68','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','Prospects','module',89,0),
	('e8dc05c0-ab78-deaf-cc6b-5ab8f7170ebe','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','AOS_Product_Categories','module',90,0),
	('e951e47a-03ec-7fdb-941a-5ab8f64e89c5','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','FP_events','module',90,0),
	('e98a4aea-ff91-ceb4-639a-5ab8f73a655e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','AOS_Quotes','module',90,0),
	('e9935d5d-afa8-1412-6824-5ab8f6507375','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOP_Case_Events','module',90,0),
	('ea00c18f-d6ef-fc23-c4a2-5ab8f72cd572','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','access','OutboundEmailAccounts','module',89,0),
	('ea0410fe-3c01-e189-2013-5ab8f6b554bd','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','Prospects','module',90,0),
	('ea24b461-a80a-52e2-58fc-5ab8f73fe99d','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','jjwg_Markers','module',90,0),
	('ea3c942f-8538-0e5e-c635-5ab8f68e4105','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','Meetings','module',90,0),
	('eb78d226-9e1c-6cc9-a2e4-5ab8f6e54420','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','Accounts','module',90,0),
	('ebf17113-58e6-e741-201f-5ab8f6234a8b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','FP_events','module',90,0),
	('ec1fc349-1e6b-0284-c4c7-5ab8f6bee9d8','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOR_Scheduled_Reports','module',90,0),
	('ec9cc0f9-3be5-0d6f-8497-5ab8f6562fd7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Project','module',90,0),
	('ed7865e3-a439-ba33-22df-5ab8f6f3f5f6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','Prospects','module',90,0),
	('ed8f5857-b585-390c-5fe9-5ab8f73ee30c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','AOS_Quotes','module',90,0),
	('edc543b4-0303-14f2-8e85-5ab8f6c693f3','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOP_Case_Events','module',90,0),
	('ede89b5d-2585-11d6-16b4-5ab8f747a363','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','import','jjwg_Markers','module',90,0),
	('edfd4b8f-b927-feea-1bb0-5ab8f7d1d41f','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','view','OutboundEmailAccounts','module',90,0),
	('eef8926b-3322-d450-f28b-5ab8f648720e','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','FP_events','module',90,0),
	('f0e76090-45aa-f8cb-0647-5ab8f6ed9326','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','Accounts','module',90,0),
	('f1734ab5-9fdd-49ca-15d7-5ab8f755a215','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOS_Quotes','module',90,0),
	('f19c0d18-b400-2aa8-2f81-5ab8f6dade3c','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','edit','Prospects','module',90,0),
	('f1bc96b5-eae3-6732-324d-5ab8f75e62cb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','jjwg_Markers','module',90,0),
	('f1d5b396-dc91-e8ec-9e87-5ab8f6b58670','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','export','AOR_Scheduled_Reports','module',90,0),
	('f2575a7e-036e-e2c2-ce23-5ab8f6fe3e51','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','massupdate','AOP_Case_Events','module',90,0),
	('f27a554d-e426-e867-07d4-5ab8f6288df6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','delete','FP_events','module',90,0),
	('f3025caa-ae02-968c-c502-5ab8f70debfb','2018-03-26 13:34:43','2018-03-26 13:34:43','1','','list','OutboundEmailAccounts','module',90,0);

/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы acl_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_roles`;

CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы acl_roles_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_roles_actions`;

CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы acl_roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_roles_users`;

CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы address_book
# ------------------------------------------------------------

DROP TABLE IF EXISTS `address_book`;

CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы alerts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alerts`;

CREATE TABLE `alerts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `target_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url_redirect` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_projecttemplates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_projecttemplates`;

CREATE TABLE `am_projecttemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `priority` varchar(100) DEFAULT 'High',
  `override_business_hours` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_projecttemplates_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_projecttemplates_audit`;

CREATE TABLE `am_projecttemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_projecttemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_projecttemplates_contacts_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_projecttemplates_contacts_1_c`;

CREATE TABLE `am_projecttemplates_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_contacts_1_alt` (`am_projecttemplates_ida`,`contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_projecttemplates_project_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_projecttemplates_project_1_c`;

CREATE TABLE `am_projecttemplates_project_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_project_1am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_projecttemplates_project_1project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_project_1_ida1` (`am_projecttemplates_project_1am_projecttemplates_ida`),
  KEY `am_projecttemplates_project_1_alt` (`am_projecttemplates_project_1project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_projecttemplates_users_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_projecttemplates_users_1_c`;

CREATE TABLE `am_projecttemplates_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_users_1_alt` (`am_projecttemplates_ida`,`users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_tasktemplates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_tasktemplates`;

CREATE TABLE `am_tasktemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `priority` varchar(100) DEFAULT 'High',
  `percent_complete` int(255) DEFAULT '0',
  `predecessors` int(255) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT '0',
  `relationship_type` varchar(100) DEFAULT 'FS',
  `task_number` int(255) DEFAULT NULL,
  `order_number` int(255) DEFAULT NULL,
  `estimated_effort` int(255) DEFAULT NULL,
  `utilization` varchar(100) DEFAULT '0',
  `duration` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_tasktemplates_am_projecttemplates_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_tasktemplates_am_projecttemplates_c`;

CREATE TABLE `am_tasktemplates_am_projecttemplates_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_tasktemplates_am_projecttemplatesam_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_tasktemplates_am_projecttemplatesam_tasktemplates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_tasktemplates_am_projecttemplates_ida1` (`am_tasktemplates_am_projecttemplatesam_projecttemplates_ida`),
  KEY `am_tasktemplates_am_projecttemplates_alt` (`am_tasktemplates_am_projecttemplatesam_tasktemplates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы am_tasktemplates_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `am_tasktemplates_audit`;

CREATE TABLE `am_tasktemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_tasktemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aobh_businesshours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aobh_businesshours`;

CREATE TABLE `aobh_businesshours` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `opening_hours` varchar(100) DEFAULT '1',
  `closing_hours` varchar(100) DEFAULT '1',
  `open` tinyint(1) DEFAULT NULL,
  `day` varchar(100) DEFAULT 'monday',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aod_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aod_index`;

CREATE TABLE `aod_index` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `last_optimised` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aod_index` WRITE;
/*!40000 ALTER TABLE `aod_index` DISABLE KEYS */;

INSERT INTO `aod_index` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `last_optimised`, `location`)
VALUES
	('1','Index','2018-03-26 13:35:18','2018-03-26 13:35:18','1','1',NULL,0,NULL,NULL,'modules/AOD_Index/Index/Index');

/*!40000 ALTER TABLE `aod_index` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы aod_index_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aod_index_audit`;

CREATE TABLE `aod_index_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_index_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aod_indexevent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aod_indexevent`;

CREATE TABLE `aod_indexevent` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `error` varchar(255) DEFAULT NULL,
  `success` tinyint(1) DEFAULT '0',
  `record_id` char(36) DEFAULT NULL,
  `record_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_record_module` (`record_module`),
  KEY `idx_record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aod_indexevent_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aod_indexevent_audit`;

CREATE TABLE `aod_indexevent_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_indexevent_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aok_knowledge_base_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aok_knowledge_base_categories`;

CREATE TABLE `aok_knowledge_base_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aok_knowledge_base_categories_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aok_knowledge_base_categories_audit`;

CREATE TABLE `aok_knowledge_base_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledge_base_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aok_knowledgebase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aok_knowledgebase`;

CREATE TABLE `aok_knowledgebase` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `revision` varchar(255) DEFAULT NULL,
  `additional_info` text,
  `user_id_c` char(36) DEFAULT NULL,
  `user_id1_c` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aok_knowledgebase_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aok_knowledgebase_audit`;

CREATE TABLE `aok_knowledgebase_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledgebase_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aok_knowledgebase_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aok_knowledgebase_categories`;

CREATE TABLE `aok_knowledgebase_categories` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aok_knowledgebase_id` varchar(36) DEFAULT NULL,
  `aok_knowledge_base_categories_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aok_knowledgebase_categories_alt` (`aok_knowledgebase_id`,`aok_knowledge_base_categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aop_case_events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aop_case_events`;

CREATE TABLE `aop_case_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aop_case_events_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aop_case_events_audit`;

CREATE TABLE `aop_case_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aop_case_updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aop_case_updates`;

CREATE TABLE `aop_case_updates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aop_case_updates_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aop_case_updates_audit`;

CREATE TABLE `aop_case_updates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_updates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_charts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_charts`;

CREATE TABLE `aor_charts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `x_field` int(11) DEFAULT NULL,
  `y_field` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_conditions`;

CREATE TABLE `aor_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `logic_op` varchar(255) DEFAULT NULL,
  `parenthesis` varchar(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `parameter` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_conditions_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_fields`;

CREATE TABLE `aor_fields` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `field_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `link` tinyint(1) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `field_function` varchar(100) DEFAULT NULL,
  `sort_by` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `sort_order` varchar(100) DEFAULT NULL,
  `group_by` tinyint(1) DEFAULT NULL,
  `group_order` varchar(100) DEFAULT NULL,
  `group_display` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_fields_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_reports`;

CREATE TABLE `aor_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `report_module` varchar(100) DEFAULT NULL,
  `graphs_per_row` int(11) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_reports_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_reports_audit`;

CREATE TABLE `aor_reports_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aor_reports_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aor_scheduled_reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aor_scheduled_reports`;

CREATE TABLE `aor_scheduled_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `schedule` varchar(100) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `email_recipients` longtext,
  `aor_report_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_contracts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_contracts`;

CREATE TABLE `aos_contracts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reference_code` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `total_contract_value` decimal(26,6) DEFAULT NULL,
  `total_contract_value_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `customer_signed_date` date DEFAULT NULL,
  `company_signed_date` date DEFAULT NULL,
  `renewal_reminder_date` datetime DEFAULT NULL,
  `contract_type` varchar(100) DEFAULT 'Type',
  `contract_account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_contracts_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_contracts_audit`;

CREATE TABLE `aos_contracts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_contracts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_contracts_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_contracts_documents`;

CREATE TABLE `aos_contracts_documents` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_contracts_id` varchar(36) DEFAULT NULL,
  `documents_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_contracts_documents_alt` (`aos_contracts_id`,`documents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_invoices`;

CREATE TABLE `aos_invoices` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `quote_number` int(11) DEFAULT NULL,
  `quote_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `template_ddown_c` text,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_invoices_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_invoices_audit`;

CREATE TABLE `aos_invoices_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_invoices_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_line_item_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_line_item_groups`;

CREATE TABLE `aos_line_item_groups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_line_item_groups_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_line_item_groups_audit`;

CREATE TABLE `aos_line_item_groups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_line_item_groups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_pdf_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_pdf_templates`;

CREATE TABLE `aos_pdf_templates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `type` varchar(100) DEFAULT NULL,
  `pdfheader` text,
  `pdffooter` text,
  `margin_left` int(255) DEFAULT '15',
  `margin_right` int(255) DEFAULT '15',
  `margin_top` int(255) DEFAULT '16',
  `margin_bottom` int(255) DEFAULT '16',
  `margin_header` int(255) DEFAULT '9',
  `margin_footer` int(255) DEFAULT '9',
  `page_size` varchar(100) DEFAULT NULL,
  `orientation` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_pdf_templates_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_pdf_templates_audit`;

CREATE TABLE `aos_pdf_templates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_pdf_templates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_product_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_product_categories`;

CREATE TABLE `aos_product_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT '0',
  `parent_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_product_categories_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_product_categories_audit`;

CREATE TABLE `aos_product_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_product_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_products`;

CREATE TABLE `aos_products` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `maincode` varchar(100) DEFAULT 'XXXX',
  `part_number` varchar(25) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT 'Good',
  `cost` decimal(26,6) DEFAULT NULL,
  `cost_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `price` decimal(26,6) DEFAULT NULL,
  `price_usdollar` decimal(26,6) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `aos_product_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_products_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_products_audit`;

CREATE TABLE `aos_products_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_products_quotes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_products_quotes`;

CREATE TABLE `aos_products_quotes` (
  `id` char(36) NOT NULL,
  `name` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `item_description` text,
  `number` int(11) DEFAULT NULL,
  `product_qty` decimal(18,4) DEFAULT NULL,
  `product_cost_price` decimal(26,6) DEFAULT NULL,
  `product_cost_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_list_price` decimal(26,6) DEFAULT NULL,
  `product_list_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount` decimal(26,6) DEFAULT NULL,
  `product_discount_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount_amount` decimal(26,6) DEFAULT NULL,
  `product_discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount` varchar(255) DEFAULT 'Percentage',
  `product_unit_price` decimal(26,6) DEFAULT NULL,
  `product_unit_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat_amt` decimal(26,6) DEFAULT NULL,
  `vat_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `product_total_price` decimal(26,6) DEFAULT NULL,
  `product_total_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat` varchar(100) DEFAULT '5.0',
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `product_id` char(36) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aospq_par_del` (`parent_id`,`parent_type`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_products_quotes_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_products_quotes_audit`;

CREATE TABLE `aos_products_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_quotes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_quotes`;

CREATE TABLE `aos_quotes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `approval_issue` text,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `number` int(11) NOT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `template_ddown_c` text,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `stage` varchar(100) DEFAULT 'Draft',
  `term` varchar(100) DEFAULT NULL,
  `terms_c` text,
  `approval_status` varchar(100) DEFAULT NULL,
  `invoice_status` varchar(100) DEFAULT 'Not Invoiced',
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_quotes_aos_invoices_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_quotes_aos_invoices_c`;

CREATE TABLE `aos_quotes_aos_invoices_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes77d9_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes6b83nvoices_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_invoices_alt` (`aos_quotes77d9_quotes_ida`,`aos_quotes6b83nvoices_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_quotes_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_quotes_audit`;

CREATE TABLE `aos_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_quotes_os_contracts_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_quotes_os_contracts_c`;

CREATE TABLE `aos_quotes_os_contracts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotese81e_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes4dc0ntracts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_contracts_alt` (`aos_quotese81e_quotes_ida`,`aos_quotes4dc0ntracts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aos_quotes_project_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aos_quotes_project_c`;

CREATE TABLE `aos_quotes_project_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes1112_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes7207project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_project_alt` (`aos_quotes1112_quotes_ida`,`aos_quotes7207project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_actions`;

CREATE TABLE `aow_actions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `action_order` int(255) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`),
  KEY `aow_action_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_conditions`;

CREATE TABLE `aow_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aow_conditions_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_processed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_processed`;

CREATE TABLE `aow_processed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `aow_processed_index_workflow` (`aow_workflow_id`,`status`,`parent_id`,`deleted`),
  KEY `aow_processed_index_status` (`status`),
  KEY `aow_processed_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_processed_aow_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_processed_aow_actions`;

CREATE TABLE `aow_processed_aow_actions` (
  `id` varchar(36) NOT NULL,
  `aow_processed_id` varchar(36) DEFAULT NULL,
  `aow_action_id` varchar(36) DEFAULT NULL,
  `status` varchar(36) DEFAULT 'Pending',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aow_processed_aow_actions` (`aow_processed_id`,`aow_action_id`),
  KEY `idx_actid_del_freid` (`aow_action_id`,`deleted`,`aow_processed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_workflow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_workflow`;

CREATE TABLE `aow_workflow` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `flow_module` varchar(100) DEFAULT NULL,
  `flow_run_on` varchar(100) DEFAULT '0',
  `status` varchar(100) DEFAULT 'Active',
  `run_when` varchar(100) DEFAULT 'Always',
  `multiple_runs` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `aow_workflow_index_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы aow_workflow_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aow_workflow_audit`;

CREATE TABLE `aow_workflow_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aow_workflow_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bugs`;

CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`),
  KEY `idx_bugs_assigned_user` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bugs_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bugs_audit`;

CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_bugs_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls`;

CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls_contacts`;

CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls_leads`;

CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls_reschedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls_reschedule`;

CREATE TABLE `calls_reschedule` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls_reschedule_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls_reschedule_audit`;

CREATE TABLE `calls_reschedule_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_calls_reschedule_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы calls_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls_users`;

CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы campaign_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `campaign_log`;

CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`),
  KEY `idx_target_id` (`target_id`),
  KEY `idx_target_id_deleted` (`target_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы campaign_trkrs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `campaign_trkrs`;

CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(255) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы campaigns
# ------------------------------------------------------------

DROP TABLE IF EXISTS `campaigns`;

CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы campaigns_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `campaigns_audit`;

CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_campaigns_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы cases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cases`;

CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL,
  `state` varchar(100) DEFAULT 'Open',
  `contact_created_by_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы cases_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cases_audit`;

CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_cases_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы cases_bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cases_bugs`;

CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы cases_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cases_cstm`;

CREATE TABLE `cases_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;

INSERT INTO `config` (`category`, `name`, `value`)
VALUES
	('notify','fromaddress','do_not_reply@example.com'),
	('notify','fromname','SuiteCRM'),
	('notify','send_by_default','1'),
	('notify','on','1'),
	('notify','send_from_assigning_user','0'),
	('info','sugar_version','6.5.25'),
	('MySettings','tab','YTozODp7aTowO3M6NDoiSG9tZSI7aToxO3M6ODoiQWNjb3VudHMiO2k6MjtzOjg6IkNvbnRhY3RzIjtpOjM7czoxMzoiT3Bwb3J0dW5pdGllcyI7aTo0O3M6NToiTGVhZHMiO2k6NTtzOjEwOiJBT1NfUXVvdGVzIjtpOjY7czo4OiJDYWxlbmRhciI7aTo3O3M6OToiRG9jdW1lbnRzIjtpOjg7czo2OiJFbWFpbHMiO2k6OTtzOjU6IlNwb3RzIjtpOjEwO3M6OToiQ2FtcGFpZ25zIjtpOjExO3M6NToiQ2FsbHMiO2k6MTI7czo4OiJNZWV0aW5ncyI7aToxMztzOjU6IlRhc2tzIjtpOjE0O3M6NToiTm90ZXMiO2k6MTU7czoxMjoiQU9TX0ludm9pY2VzIjtpOjE2O3M6MTM6IkFPU19Db250cmFjdHMiO2k6MTc7czo1OiJDYXNlcyI7aToxODtzOjk6IlByb3NwZWN0cyI7aToxOTtzOjEzOiJQcm9zcGVjdExpc3RzIjtpOjIwO3M6NzoiUHJvamVjdCI7aToyMTtzOjE5OiJBTV9Qcm9qZWN0VGVtcGxhdGVzIjtpOjIyO3M6MTY6IkFNX1Rhc2tUZW1wbGF0ZXMiO2k6MjM7czo5OiJGUF9ldmVudHMiO2k6MjQ7czoxODoiRlBfRXZlbnRfTG9jYXRpb25zIjtpOjI1O3M6MTI6IkFPU19Qcm9kdWN0cyI7aToyNjtzOjIyOiJBT1NfUHJvZHVjdF9DYXRlZ29yaWVzIjtpOjI3O3M6MTc6IkFPU19QREZfVGVtcGxhdGVzIjtpOjI4O3M6OToiamp3Z19NYXBzIjtpOjI5O3M6MTI6Impqd2dfTWFya2VycyI7aTozMDtzOjEwOiJqandnX0FyZWFzIjtpOjMxO3M6MTg6Impqd2dfQWRkcmVzc19DYWNoZSI7aTozMjtzOjExOiJBT1JfUmVwb3J0cyI7aTozMztzOjEyOiJBT1dfV29ya0Zsb3ciO2k6MzQ7czoxNzoiQU9LX0tub3dsZWRnZUJhc2UiO2k6MzU7czoyOToiQU9LX0tub3dsZWRnZV9CYXNlX0NhdGVnb3JpZXMiO2k6MzY7czoxNDoiRW1haWxUZW1wbGF0ZXMiO2k6Mzc7czo3OiJTdXJ2ZXlzIjt9'),
	('portal','on','0'),
	('tracker','Tracker','1'),
	('system','skypeout_on','1'),
	('sugarfeed','enabled','1'),
	('sugarfeed','module_UserFeed','1'),
	('sugarfeed','module_Opportunities','1'),
	('sugarfeed','module_Leads','1'),
	('sugarfeed','module_Contacts','1'),
	('sugarfeed','module_Cases','1'),
	('Update','CheckUpdates','manual'),
	('system','name','SuiteCRM'),
	('system','adminwizard','1'),
	('notify','allow_default_outbound','0');

/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `joomla_account_id` varchar(255) DEFAULT NULL,
  `portal_account_disabled` tinyint(1) DEFAULT NULL,
  `portal_user_type` varchar(100) DEFAULT 'Single',
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы contacts_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts_audit`;

CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_contacts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы contacts_bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts_bugs`;

CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы contacts_cases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts_cases`;

CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы contacts_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts_cstm`;

CREATE TABLE `contacts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы contacts_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts_users`;

CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы cron_remove_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cron_remove_documents`;

CREATE TABLE `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  KEY `idx_cron_remove_document_stamp` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы currencies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы custom_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `custom_fields`;

CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы document_revisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_revisions`;

CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentrevision_mimetype` (`file_mime_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_accounts`;

CREATE TABLE `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  KEY `documents_accounts_document_id` (`document_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents_bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_bugs`;

CREATE TABLE `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  KEY `documents_bugs_document_id` (`document_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents_cases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_cases`;

CREATE TABLE `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_cases_case_id` (`case_id`,`document_id`),
  KEY `documents_cases_document_id` (`document_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_contacts`;

CREATE TABLE `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  KEY `documents_contacts_document_id` (`document_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы documents_opportunities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents_opportunities`;

CREATE TABLE `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы eapm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `eapm`;

CREATE TABLE `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы email_addr_bean_rel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_addr_bean_rel`;

CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `email_addr_bean_rel` WRITE;
/*!40000 ALTER TABLE `email_addr_bean_rel` DISABLE KEYS */;

INSERT INTO `email_addr_bean_rel` (`id`, `email_address_id`, `bean_id`, `bean_module`, `primary_address`, `reply_to_address`, `date_created`, `date_modified`, `deleted`)
VALUES
	('b5c16453-a4a4-93ed-0905-5ab8f7f6459d','b5d8b1e9-a2a3-235c-d85a-5ab8f7615aac','1','Users',1,0,'2018-03-26 13:34:43','2018-03-26 13:34:43',0);

/*!40000 ALTER TABLE `email_addr_bean_rel` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы email_addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_addresses`;

CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `confirm_opt_in` varchar(255) DEFAULT 'not-opt-in',
  `confirm_opt_in_date` datetime DEFAULT NULL,
  `confirm_opt_in_sent_date` datetime DEFAULT NULL,
  `confirm_opt_in_fail_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `email_addresses` WRITE;
/*!40000 ALTER TABLE `email_addresses` DISABLE KEYS */;

INSERT INTO `email_addresses` (`id`, `email_address`, `email_address_caps`, `invalid_email`, `opt_out`, `confirm_opt_in`, `confirm_opt_in_date`, `confirm_opt_in_sent_date`, `confirm_opt_in_fail_date`, `date_created`, `date_modified`, `deleted`)
VALUES
	('b5d8b1e9-a2a3-235c-d85a-5ab8f7615aac','karpushchenko@mail.ru','KARPUSHCHENKO@MAIL.RU',0,0,'not-opt-in',NULL,NULL,NULL,'2018-03-26 13:34:43','2018-03-26 13:34:43',0);

/*!40000 ALTER TABLE `email_addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы email_addresses_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_addresses_audit`;

CREATE TABLE `email_addresses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_email_addresses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы email_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_cache`;

CREATE TABLE `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned DEFAULT NULL,
  `imap_uid` int(10) unsigned DEFAULT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`),
  KEY `idx_mail_to` (`toaddr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы email_marketing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_marketing`;

CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `outbound_email_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы email_marketing_prospect_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;

CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` longtext,
  `body_html` longtext,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;

INSERT INTO `email_templates` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `published`, `name`, `description`, `subject`, `body`, `body_html`, `deleted`, `assigned_user_id`, `text_only`, `type`)
VALUES
	('4f20aff2-271f-e6e9-4c66-5ab8f7349358','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','System-generated password email','This template is used when the System Administrator sends a new password to a user.','New account information','\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.','<div><table width=\"550\"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system'),
	('56c07dd9-f64f-46f9-c4f3-5ab8f7beb4db','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Forgot Password email','This template is used to send a user a link to click to reset the user\'s account password.','Reset your account password','\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid','<div><table width=\"550\"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system'),
	('6154b06b-4825-3455-42ce-5ab8f7933a80','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Two Factor Authentication email','This template is used to send a user a code for Two Factor Authentication.','Two Factor Authentication Code','Two Factor Authentication code is $code.','<div><table width=\"550\"><tbody><tr><td><p>Two Factor Authentication code is <b>$code</b>.</p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system'),
	('8902ca2e-41de-9c66-fffd-5ab8f7733fa6','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Case Closure','Template for informing a contact that their case has been closed.','$acase_name [CASE:$acase_case_number] closed','Hi $contact_first_name $contact_last_name,\n\n					   Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered\n					   Status:				$acase_status\n					   Reference:			$acase_case_number\n					   Resolution:			$acase_resolution','<p> Hi $contact_first_name $contact_last_name,</p>\n					    <p>Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered</p>\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Resolution</td><td>$acase_resolution</td></tr></tbody></table>',0,NULL,NULL,'system'),
	('905e38b7-5f52-b554-3a56-5ab8f72ae580','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Joomla Account Creation','Template used when informing a contact that they\'ve been given an account on the joomla portal.','Support Portal Account Created','Hi $contact_name,\n					   An account has been created for you at $portal_address.\n					   You may login using this email address and the password $joomla_pass','<p>Hi $contact_name,</p>\n					    <p>An account has been created for you at <a href=\"$portal_address\">$portal_address</a>.</p>\n					    <p>You may login using this email address and the password $joomla_pass</p>',0,NULL,NULL,'system'),
	('996ce51c-75d2-375b-b68d-5ab8f7406e7b','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Case Creation','Template to send to a contact when a case is received from them.','$acase_name [CASE:$acase_case_number]','Hi $contact_first_name $contact_last_name,\n\n					   We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered\n					   Status:		$acase_status\n					   Reference:	$acase_case_number\n					   Description:	$acase_description','<p> Hi $contact_first_name $contact_last_name,</p>\n					    <p>We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered</p>\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Description</td><td>$acase_description</td></tr></tbody></table>',0,NULL,NULL,'system'),
	('a13fc162-42e8-60e9-4f8c-5ab8f7b3d782','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Contact Case Update','Template to send to a contact when their case is updated.','$acase_name update [CASE:$acase_case_number]','Hi $user_first_name $user_last_name,\n\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\n					       $contact_first_name $contact_last_name, said:\n					               $aop_case_updates_description','<p>Hi $contact_first_name $contact_last_name,</p>\n					    <p> </p>\n					    <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\n					    <p><strong>$user_first_name $user_last_name said:</strong></p>\n					    <p style=\"padding-left:30px;\">$aop_case_updates_description</p>',0,NULL,NULL,'system'),
	('ac253768-6a30-1bc3-5989-5ab8f726fc18','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','User Case Update','Email template to send to a Sugar user when their case is updated.','$acase_name (# $acase_case_number) update','Hi $user_first_name $user_last_name,\n\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\n					       $contact_first_name $contact_last_name, said:\n					               $aop_case_updates_description\n                        You may review this Case at:\n                            $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;','<p>Hi $user_first_name $user_last_name,</p>\n					     <p> </p>\n					     <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\n					     <p><strong>$contact_first_name $contact_last_name, said:</strong></p>\n					     <p style=\"padding-left:30px;\">$aop_case_updates_description</p>\n					     <p>You may review this Case at: $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;</p>',0,NULL,NULL,'system'),
	('b4a18dce-edde-e7f1-6974-5ab8f7d387c7','2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','off','Confirmed Opt In','Email template to send to a contact to confirm they have opted in.','Confirm Opt In','Hi $contact_first_name $contact_last_name, \\n Please confirm that you have opted in by selecting the following link: $sugarurl/index.php?entryPoint=ConfirmOptIn&from=$emailaddress_email_address','<p>Hi $contact_first_name $contact_last_name,</p>\n             <p>\n                Please confirm that you have opted in by selecting the following link:\n                <a href=\"$sugarurl/index.php?entryPoint=ConfirmOptIn&from=$emailaddress_email_address\">Opt In</a>\n             </p>',0,NULL,NULL,'system'),
	('bd19ad3a-0522-31f0-f458-5ab8f7567cc9','2013-05-24 14:31:45','2018-03-26 13:34:43','1','1','off','Event Invite Template','Default event invite template.','You have been invited to $fp_events_name','Dear $contact_name,\nYou have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end\n$fp_events_description\nYours Sincerely,\n','\n<p>Dear $contact_name,</p>\n<p>You have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end</p>\n<p>$fp_events_description</p>\n<p>If you would like to accept this invititation please click accept.</p>\n<p> $fp_events_link or $fp_events_link_declined</p>\n<p>Yours Sincerely,</p>\n',0,NULL,NULL,'system');

/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы emailman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailman`;

CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `related_confirm_opt_in` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emails`;

CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `orphaned` tinyint(1) DEFAULT NULL,
  `last_synced` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`),
  KEY `idx_email_cat` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы emails_beans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emails_beans`;

CREATE TABLE `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы emails_email_addr_rel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emails_email_addr_rel`;

CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы emails_text
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emails_text`;

CREATE TABLE `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Дамп таблицы favorites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `favorites`;

CREATE TABLE `favorites` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fields_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fields_meta_data`;

CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fields_meta_data` WRITE;
/*!40000 ALTER TABLE `fields_meta_data` DISABLE KEYS */;

INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`)
VALUES
	('Accountsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Accounts','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Accountsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Accounts','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Accountsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Accounts','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Accountsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Accounts','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Casesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Cases','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Casesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Cases','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Casesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Cases','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Casesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Cases','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Contactsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Contacts','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Contactsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Contacts','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Contactsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Contacts','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Contactsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Contacts','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Leadsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Leads','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Leadsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Leads','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Leadsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Leads','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Leadsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Leads','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Meetingsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Meetings','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Meetingsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Meetings','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Meetingsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Meetings','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Meetingsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Meetings','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Opportunitiesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Opportunities','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Opportunitiesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Opportunities','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Opportunitiesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Opportunities','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Opportunitiesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Opportunities','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Projectjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Project','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Projectjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Project','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Projectjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Project','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Projectjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Project','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Prospectsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Prospects','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Prospectsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Prospects','varchar',255,0,NULL,'2018-03-26 13:34:43',0,0,0,0,1,'true',NULL,'','',''),
	('Prospectsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Prospects','float',10,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','',''),
	('Prospectsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Prospects','float',11,0,'0.00000000','2018-03-26 13:34:43',0,0,0,0,1,'true','8','','','');

/*!40000 ALTER TABLE `fields_meta_data` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `folders`;

CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы folders_rel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `folders_rel`;

CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы folders_subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `folders_subscriptions`;

CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_event_locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_event_locations`;

CREATE TABLE `fp_event_locations` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_event_locations_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_event_locations_audit`;

CREATE TABLE `fp_event_locations_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_event_locations_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_event_locations_fp_events_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_event_locations_fp_events_1_c`;

CREATE TABLE `fp_event_locations_fp_events_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_event_locations_fp_events_1fp_event_locations_ida` varchar(36) DEFAULT NULL,
  `fp_event_locations_fp_events_1fp_events_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_event_locations_fp_events_1_ida1` (`fp_event_locations_fp_events_1fp_event_locations_ida`),
  KEY `fp_event_locations_fp_events_1_alt` (`fp_event_locations_fp_events_1fp_events_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events`;

CREATE TABLE `fp_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `budget` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `invite_templates` varchar(100) DEFAULT NULL,
  `accept_redirect` varchar(255) DEFAULT NULL,
  `decline_redirect` varchar(255) DEFAULT NULL,
  `activity_status_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_audit`;

CREATE TABLE `fp_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_contacts_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_contacts_c`;

CREATE TABLE `fp_events_contacts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_contactsfp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_contactscontacts_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_contacts_alt` (`fp_events_contactsfp_events_ida`,`fp_events_contactscontacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_fp_event_delegates_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_fp_event_delegates_1_c`;

CREATE TABLE `fp_events_fp_event_delegates_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_delegates_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_delegates_1fp_event_delegates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_delegates_1_ida1` (`fp_events_fp_event_delegates_1fp_events_ida`),
  KEY `fp_events_fp_event_delegates_1_alt` (`fp_events_fp_event_delegates_1fp_event_delegates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_fp_event_locations_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_fp_event_locations_1_c`;

CREATE TABLE `fp_events_fp_event_locations_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_locations_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_locations_1fp_event_locations_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_locations_1_alt` (`fp_events_fp_event_locations_1fp_events_ida`,`fp_events_fp_event_locations_1fp_event_locations_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_leads_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_leads_1_c`;

CREATE TABLE `fp_events_leads_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_leads_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_leads_1leads_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_leads_1_alt` (`fp_events_leads_1fp_events_ida`,`fp_events_leads_1leads_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы fp_events_prospects_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fp_events_prospects_1_c`;

CREATE TABLE `fp_events_prospects_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_prospects_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_prospects_1prospects_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_prospects_1_alt` (`fp_events_prospects_1fp_events_ida`,`fp_events_prospects_1prospects_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы import_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `import_maps`;

CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы inbound_email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inbound_email`;

CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы inbound_email_autoreply
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inbound_email_autoreply`;

CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы inbound_email_cache_ts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inbound_email_cache_ts`;

CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_address_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_address_cache`;

CREATE TABLE `jjwg_address_cache` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `lat` float(10,8) DEFAULT NULL,
  `lng` float(11,8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_address_cache_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_address_cache_audit`;

CREATE TABLE `jjwg_address_cache_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_address_cache_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_areas`;

CREATE TABLE `jjwg_areas` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `coordinates` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_areas_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_areas_audit`;

CREATE TABLE `jjwg_areas_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_areas_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_maps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_maps`;

CREATE TABLE `jjwg_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `distance` float(9,4) DEFAULT NULL,
  `unit_type` varchar(100) DEFAULT 'mi',
  `module_type` varchar(100) DEFAULT 'Accounts',
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_maps_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_maps_audit`;

CREATE TABLE `jjwg_maps_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_maps_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_maps_jjwg_areas_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_maps_jjwg_areas_c`;

CREATE TABLE `jjwg_maps_jjwg_areas_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_5304wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_41f2g_areas_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_areas_alt` (`jjwg_maps_5304wg_maps_ida`,`jjwg_maps_41f2g_areas_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_maps_jjwg_markers_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_maps_jjwg_markers_c`;

CREATE TABLE `jjwg_maps_jjwg_markers_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_b229wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_2e31markers_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_markers_alt` (`jjwg_maps_b229wg_maps_ida`,`jjwg_maps_2e31markers_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_markers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_markers`;

CREATE TABLE `jjwg_markers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `jjwg_maps_lat` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_lng` float(11,8) DEFAULT '0.00000000',
  `marker_image` varchar(100) DEFAULT 'company',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы jjwg_markers_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jjwg_markers_audit`;

CREATE TABLE `jjwg_markers_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_markers_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы job_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_queue`;

CREATE TABLE `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  KEY `idx_status_entered` (`status`,`date_entered`),
  KEY `idx_status_modified` (`status`,`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`),
  KEY `idx_reports_to` (`reports_to_id`),
  KEY `idx_lead_phone_work` (`phone_work`),
  KEY `idx_leads_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы leads_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `leads_audit`;

CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_leads_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы leads_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `leads_cstm`;

CREATE TABLE `leads_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы linked_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `linked_documents`;

CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы meetings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings`;

CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  KEY `idx_meet_date_start` (`date_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы meetings_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings_contacts`;

CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы meetings_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings_cstm`;

CREATE TABLE `meetings_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы meetings_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings_leads`;

CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы meetings_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings_users`;

CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`),
  KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы oauth_consumer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_consumer`;

CREATE TABLE `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ckey` (`c_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы oauth_nonce
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_nonce`;

CREATE TABLE `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`conskey`,`nonce`),
  KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы oauth_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_tokens`;

CREATE TABLE `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`,`deleted`),
  KEY `oauth_state_ts` (`tstate`,`token_ts`),
  KEY `constoken_key` (`consumer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы oauth2clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth2clients`;

CREATE TABLE `oauth2clients` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `secret` varchar(4000) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `is_confidential` tinyint(1) DEFAULT '1',
  `allowed_grant_type` varchar(255) DEFAULT 'password',
  `duration_value` int(11) DEFAULT NULL,
  `duration_amount` int(11) DEFAULT NULL,
  `duration_unit` varchar(255) DEFAULT 'Duration Unit',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы oauth2tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth2tokens`;

CREATE TABLE `oauth2tokens` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `token_is_revoked` tinyint(1) DEFAULT NULL,
  `token_type` varchar(255) DEFAULT NULL,
  `access_token_expires` datetime DEFAULT NULL,
  `access_token` varchar(4000) DEFAULT NULL,
  `refresh_token` varchar(4000) DEFAULT NULL,
  `refresh_token_expires` datetime DEFAULT NULL,
  `grant_type` varchar(255) DEFAULT NULL,
  `state` varchar(1024) DEFAULT NULL,
  `client` char(36) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы opportunities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opportunities`;

CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_opp_id_deleted` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы opportunities_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opportunities_audit`;

CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opportunities_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы opportunities_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opportunities_contacts`;

CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы opportunities_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `opportunities_cstm`;

CREATE TABLE `opportunities_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы outbound_email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `outbound_email`;

CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `smtp_from_name` varchar(255) DEFAULT NULL,
  `smtp_from_addr` varchar(255) DEFAULT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` varchar(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `outbound_email` WRITE;
/*!40000 ALTER TABLE `outbound_email` DISABLE KEYS */;

INSERT INTO `outbound_email` (`id`, `name`, `type`, `user_id`, `smtp_from_name`, `smtp_from_addr`, `mail_sendtype`, `mail_smtptype`, `mail_smtpserver`, `mail_smtpport`, `mail_smtpuser`, `mail_smtppass`, `mail_smtpauth_req`, `mail_smtpssl`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `deleted`, `assigned_user_id`)
VALUES
	('9ddcf8aa-8867-0d19-90f9-5ab8f7636bec','system','system','1',NULL,NULL,'SMTP','other','',25,'','',1,'0',NULL,NULL,NULL,NULL,0,NULL);

/*!40000 ALTER TABLE `outbound_email` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы outbound_email_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `outbound_email_audit`;

CREATE TABLE `outbound_email_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_outbound_email_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `override_business_hours` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project_contacts_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_contacts_1_c`;

CREATE TABLE `project_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_contacts_1project_ida` varchar(36) DEFAULT NULL,
  `project_contacts_1contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_contacts_1_alt` (`project_contacts_1project_ida`,`project_contacts_1contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_cstm`;

CREATE TABLE `project_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project_task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_task`;

CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `relationship_type` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project_task_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_task_audit`;

CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_project_task_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы project_users_1_c
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_users_1_c`;

CREATE TABLE `project_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_users_1project_ida` varchar(36) DEFAULT NULL,
  `project_users_1users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_users_1_alt` (`project_users_1project_ida`,`project_users_1users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_accounts`;

CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_bugs`;

CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_cases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_cases`;

CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_contacts`;

CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_opportunities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_opportunities`;

CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы projects_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects_products`;

CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы prospect_list_campaigns
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospect_list_campaigns`;

CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы prospect_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospect_lists`;

CREATE TABLE `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы prospect_lists_prospects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospect_lists_prospects`;

CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы prospects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospects`;

CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  KEY `idx_prospects_id_del` (`id`,`deleted`),
  KEY `idx_prospects_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы prospects_cstm
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prospects_cstm`;

CREATE TABLE `prospects_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relationships`;

CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(64) DEFAULT NULL,
  `join_key_lhs` varchar(64) DEFAULT NULL,
  `join_key_rhs` varchar(64) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `relationships` WRITE;
/*!40000 ALTER TABLE `relationships` DISABLE KEYS */;

INSERT INTO `relationships` (`id`, `relationship_name`, `lhs_module`, `lhs_table`, `lhs_key`, `rhs_module`, `rhs_table`, `rhs_key`, `join_table`, `join_key_lhs`, `join_key_rhs`, `relationship_type`, `relationship_role_column`, `relationship_role_column_value`, `reverse`, `deleted`)
VALUES
	('1069eddb-d7ef-4d6c-388e-5ab8ff187ba0','tasks_modified_user','Users','users','id','Tasks','tasks','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('10c00785-ee2c-1e6a-b529-5ab8ffc59603','documents_cases','Documents','documents','id','Cases','cases','id','documents_cases','document_id','case_id','many-to-many',NULL,NULL,0,0),
	('10e30011-2442-ac2e-5ba2-5ab8ff0bfa1b','tasks_created_by','Users','users','id','Tasks','tasks','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('113907b7-47f0-74e8-41c8-5ab8ff76273f','aok_knowledgebase_categories','AOK_KnowledgeBase','aok_knowledgebase','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','id','aok_knowledgebase_categories','aok_knowledgebase_id','aok_knowledge_base_categories_id','many-to-many',NULL,NULL,0,0),
	('11c91f04-f2c8-4e5f-f119-5ab8ff6caec4','tasks_assigned_user','Users','users','id','Tasks','tasks','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('12342a16-b08d-a714-e9ca-5ab8ff0d48c3','securitygroups_tasks','SecurityGroups','securitygroups','id','Tasks','tasks','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Tasks',0,0),
	('129d52d8-fc6b-f251-1942-5ab8fffbca64','tasks_notes','Tasks','tasks','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('12d00727-697b-494b-41c1-5ab8ff222da7','calls_notes','Calls','calls','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Calls',0,0),
	('152cb732-7681-a0d7-d094-5ab8ffe179ab','aor_scheduled_reports_modified_user','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('15f75410-c93b-fd1b-e3c3-5ab8ffaf92a4','aor_scheduled_reports_created_by','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('18020a29-db1f-e520-0e2f-5ab8ffbe46b2','am_projecttemplates_project_1','AM_ProjectTemplates','am_projecttemplates','id','Project','project','id','am_projecttemplates_project_1_c','am_projecttemplates_project_1am_projecttemplates_ida','am_projecttemplates_project_1project_idb','many-to-many',NULL,NULL,0,0),
	('18730a03-9a88-6a9c-6c0a-5ab8fff74f4d','leads_modified_user','Users','users','id','Leads','leads','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1a0666ab-3874-a82e-a108-5ab8ff001869','leads_created_by','Users','users','id','Leads','leads','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1a23b8e4-2432-cb0a-1832-5ab8ff3d09cf','aos_contracts_modified_user','Users','users','id','AOS_Contracts','aos_contracts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1a90fe31-c3bd-00ce-3fec-5ab8ff8db7bc','leads_assigned_user','Users','users','id','Leads','leads','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1ab730da-26b3-3f88-fffe-5ab8ffcb577c','aos_contracts_created_by','Users','users','id','AOS_Contracts','aos_contracts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1b2cb44a-021a-913a-63e9-5ab8ff8ae42a','aos_contracts_assigned_user','Users','users','id','AOS_Contracts','aos_contracts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1b4996da-0408-827e-d4ad-5ab8ff5aa936','securitygroups_leads','SecurityGroups','securitygroups','id','Leads','leads','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Leads',0,0),
	('1baf487b-6b37-f395-29da-5ab8ff052c72','securitygroups_aos_contracts','SecurityGroups','securitygroups','id','AOS_Contracts','aos_contracts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Contracts',0,0),
	('1bc1c7e2-838d-829c-713c-5ab8ff1eb8c2','alerts_modified_user','Users','users','id','Alerts','alerts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1bdf55de-ce74-7d33-0273-5ab8ff308b8a','leads_email_addresses','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Leads',0,0),
	('1c20e475-7ab9-c8a0-868e-5ab8ff35a19e','aos_contracts_tasks','AOS_Contracts','aos_contracts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),
	('1c316e42-7960-c149-e507-5ab8ffc29945','alerts_created_by','Users','users','id','Alerts','alerts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1c4dd785-91c7-d8c5-8243-5ab8ffda47c1','leads_email_addresses_primary','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),
	('1c930f0d-451c-d034-ec0a-5ab8ffe4dab7','alerts_assigned_user','Users','users','id','Alerts','alerts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1c99822e-77c8-3c95-b3de-5ab8ff6d7db3','aos_contracts_notes','AOS_Contracts','aos_contracts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),
	('1d133bc1-00ad-b1ec-1148-5ab8ff594290','aos_contracts_meetings','AOS_Contracts','aos_contracts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),
	('1e15aa92-c72b-9294-459a-5ab8ffe66081','aos_contracts_calls','AOS_Contracts','aos_contracts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),
	('1ed80109-3a28-f7b5-af40-5ab8ffd81d67','am_projecttemplates_contacts_1','AM_ProjectTemplates','am_projecttemplates','id','Contacts','contacts','id','am_projecttemplates_contacts_1_c','am_projecttemplates_ida','contacts_idb','many-to-many',NULL,NULL,0,0),
	('1ef5c5ef-2830-5007-e3e3-5ab8ff13f415','aos_contracts_aos_products_quotes','AOS_Contracts','aos_contracts','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('1f7e05fb-0a67-fe2e-90a0-5ab8ffbd95ec','aos_contracts_aos_line_item_groups','AOS_Contracts','aos_contracts','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('20bfcec9-5593-0d7e-c74c-5ab8ff64dbe7','documents_modified_user','Users','users','id','Documents','documents','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2151e0cf-c71a-4c4d-e883-5ab8ff2746fa','documents_created_by','Users','users','id','Documents','documents','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('21eff8b8-813d-e29e-203e-5ab8ff2424f8','documents_assigned_user','Users','users','id','Documents','documents','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('228cfb9f-9965-5ff1-a810-5ab8ffc8342d','securitygroups_documents','SecurityGroups','securitygroups','id','Documents','documents','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Documents',0,0),
	('22b302c9-601d-7d48-a9eb-5ab8ff7e8f95','aos_invoices_modified_user','Users','users','id','AOS_Invoices','aos_invoices','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('231d6e7a-4e14-f650-04e6-5ab8ff955610','document_revisions','Documents','documents','id','DocumentRevisions','document_revisions','document_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2326f023-4e15-8abe-e34a-5ab8fff3ddb5','aos_invoices_created_by','Users','users','id','AOS_Invoices','aos_invoices','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('23a109cc-cf87-59b1-d873-5ab8ffc38c3c','aos_invoices_assigned_user','Users','users','id','AOS_Invoices','aos_invoices','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('240fa8d0-5c32-7cd3-37ea-5ab8ff235daf','securitygroups_aos_invoices','SecurityGroups','securitygroups','id','AOS_Invoices','aos_invoices','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Invoices',0,0),
	('2486d74d-60db-433f-1b10-5ab8ff70e1d3','aos_invoices_aos_product_quotes','AOS_Invoices','aos_invoices','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('24904b83-25c9-f987-24a4-5ab8ff810455','revisions_created_by','Users','users','id','DocumentRevisions','document_revisions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('24d2db4d-d115-29ac-86cf-5ab8ffa7df36','lead_direct_reports','Leads','leads','id','Leads','leads','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('24d4a179-7b6f-79d0-c041-5ab8ff5eb270','aos_invoices_aos_line_item_groups','AOS_Invoices','aos_invoices','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('26160889-0dcf-4a6e-1168-5ab8ff3e4559','am_projecttemplates_users_1','AM_ProjectTemplates','am_projecttemplates','id','Users','users','id','am_projecttemplates_users_1_c','am_projecttemplates_ida','users_idb','many-to-many',NULL,NULL,0,0),
	('26648979-4e06-cca8-8961-5ab8ff172d5d','lead_tasks','Leads','leads','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('26f5953a-6f23-910c-3529-5ab8ff57a918','lead_notes','Leads','leads','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('27566196-8684-ccfd-5fc2-5ab8ff07b2b6','lead_meetings','Leads','leads','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('27c004eb-16a8-595b-339b-5ab8ff0125af','lead_calls','Leads','leads','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('27f96810-00a3-f949-6e6d-5ab8ff37537f','aos_pdf_templates_modified_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('281c0013-da11-67fd-2a8e-5ab8ff181503','emails_modified_user','Users','users','id','Emails','emails','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('28298f7e-143d-aeeb-425b-5ab8ff882180','lead_emails','Leads','leads','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('2871b702-bc7d-a74b-7129-5ab8fffa9293','aos_pdf_templates_created_by','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2896832a-9b3d-9913-9d63-5ab8ff813dab','lead_campaign_log','Leads','leads','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Leads',0,0),
	('290feb7b-7eaf-a70b-2fbf-5ab8ff2ca9c7','aos_pdf_templates_assigned_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('29b09cac-2926-1a36-5956-5ab8ffaf1987','securitygroups_aos_pdf_templates','SecurityGroups','securitygroups','id','AOS_PDF_Templates','aos_pdf_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_PDF_Templates',0,0),
	('2b9e0756-f031-2c71-e6cf-5ab8ff458ec9','emails_created_by','Users','users','id','Emails','emails','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2bde1202-74d1-0325-c531-5ab8ff5a4a0e','aos_product_categories_modified_user','Users','users','id','AOS_Product_Categories','aos_product_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2c4f9e09-2d43-46db-5f85-5ab8ff549bf9','aos_product_categories_created_by','Users','users','id','AOS_Product_Categories','aos_product_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2cdfe03c-b85a-0b8b-f506-5ab8ff8765e9','aos_product_categories_assigned_user','Users','users','id','AOS_Product_Categories','aos_product_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('2d581b7c-b039-5c0f-4970-5ab8fffac90b','securitygroups_aos_product_categories','SecurityGroups','securitygroups','id','AOS_Product_Categories','aos_product_categories','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Product_Categories',0,0),
	('2dbf04bd-ec2e-b886-6dda-5ab8ff9d1783','am_tasktemplates_am_projecttemplates','AM_ProjectTemplates','am_projecttemplates','id','AM_TaskTemplates','am_tasktemplates','id','am_tasktemplates_am_projecttemplates_c','am_tasktemplates_am_projecttemplatesam_projecttemplates_ida','am_tasktemplates_am_projecttemplatesam_tasktemplates_idb','many-to-many',NULL,NULL,0,0),
	('2dcf6598-f155-672d-672a-5ab8ff4f15d2','sub_product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Product_Categories','aos_product_categories','parent_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('30be6a32-18c5-1bd2-8ea5-5ab8ff463ea9','cases_modified_user','Users','users','id','Cases','cases','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('32210aef-66df-7ed4-6092-5ab8fff5dfa7','emails_assigned_user','Users','users','id','Emails','emails','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3235beb1-17d8-a76b-ca3b-5ab8ff9f71ac','cases_created_by','Users','users','id','Cases','cases','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('32a272e9-a1dd-594d-c1b3-5ab8ff0687ca','cases_assigned_user','Users','users','id','Cases','cases','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('33068489-7401-6835-dc5a-5ab8ff98c1b8','securitygroups_cases','SecurityGroups','securitygroups','id','Cases','cases','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Cases',0,0),
	('33736788-c428-86bf-605b-5ab8ff5c1c40','case_calls','Cases','cases','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('33ea4c91-9646-9702-6e6c-5ab8ffba7653','case_tasks','Cases','cases','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('3465b125-35be-9481-071b-5ab8ff6c6880','case_notes','Cases','cases','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('34caf25c-88d7-72cf-ed4c-5ab8ff5afa0f','case_meetings','Cases','cases','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('358c07ca-9e09-6e54-dbe3-5ab8ff8665e4','aos_contracts_documents','AOS_Contracts','aos_contracts','id','Documents','documents','id','aos_contracts_documents','aos_contracts_id','documents_id','many-to-many',NULL,NULL,0,0),
	('35fa1f8e-6667-3c10-44ac-5ab8ffaacc47','inbound_email_created_by','Users','users','id','InboundEmail','inbound_email','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),
	('360884af-94a6-2582-6a71-5ab8ffc004af','case_emails','Cases','cases','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('36765c18-7f39-0682-56a7-5ab8ff9d66d8','inbound_email_modified_user_id','Users','users','id','InboundEmail','inbound_email','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),
	('3691ea6f-108b-0c7c-eea8-5ab8ff6cd16e','cases_created_contact','Contacts','contacts','id','Cases','cases','contact_created_by_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('38db011c-55e4-e84f-c30e-5ab8ff90c024','securitygroups_emails','SecurityGroups','securitygroups','id','Emails','emails','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Emails',0,0),
	('390cf94b-b93f-9085-b67a-5ab8ffef88db','saved_search_assigned_user','Users','users','id','SavedSearch','saved_search','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3a80b819-80b8-6773-4440-5ab8ff97f2ab','aos_products_modified_user','Users','users','id','AOS_Products','aos_products','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3c1f8e9f-cc3b-37dc-9511-5ab8ffb96c59','aos_products_created_by','Users','users','id','AOS_Products','aos_products','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3c3827dd-2877-d9e3-aac6-5ab8ff047966','bugs_modified_user','Users','users','id','Bugs','bugs','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3ca921f1-575c-251b-737d-5ab8ff80ffc7','bugs_created_by','Users','users','id','Bugs','bugs','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3cb14ac1-3e77-7d05-356c-5ab8ffe8df9d','aos_products_assigned_user','Users','users','id','AOS_Products','aos_products','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3d17c751-fed4-86d8-73fb-5ab8ffca6ae2','bugs_assigned_user','Users','users','id','Bugs','bugs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3d31ced9-1ef4-5b7e-ad2f-5ab8ff9bea9d','securitygroups_aos_products','SecurityGroups','securitygroups','id','AOS_Products','aos_products','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Products',0,0),
	('3db74d34-4c21-1b44-48c6-5ab8ff216edf','product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Products','aos_products','aos_product_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3e19dbce-3002-5b7f-8842-5ab8ff61f086','spots_modified_user','Users','users','id','Spots','spots','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3e4ffcdf-9c98-b7a1-c613-5ab8ffbefd9b','securitygroups_bugs','SecurityGroups','securitygroups','id','Bugs','bugs','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Bugs',0,0),
	('3e840984-a1fb-a19b-a0e1-5ab8ff6ac3f5','aos_quotes_aos_contracts','AOS_Quotes','aos_quotes','id','AOS_Contracts','aos_contracts','id','aos_quotes_os_contracts_c','aos_quotese81e_quotes_ida','aos_quotes4dc0ntracts_idb','many-to-many',NULL,NULL,0,0),
	('3eae589b-32da-2e29-d0e5-5ab8ff2399e4','spots_created_by','Users','users','id','Spots','spots','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3ecb78e3-304c-0d87-ffd7-5ab8ffde9ebd','bug_tasks','Bugs','bugs','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),
	('3f1c8548-733f-a933-3ec4-5ab8ff1777ff','spots_assigned_user','Users','users','id','Spots','spots','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('3f7af4d9-bff0-9b35-d49a-5ab8fff5f3a8','bug_meetings','Bugs','bugs','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),
	('3fbe5065-be17-2a93-3b14-5ab8ffc4671b','securitygroups_spots','SecurityGroups','securitygroups','id','Spots','spots','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Spots',0,0),
	('40eb5e60-f0d4-d87c-f238-5ab8ff0ca1e7','aos_products_quotes_modified_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('41350ac9-723a-6a56-140e-5ab8ff3706bf','emails_notes_rel','Emails','emails','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4170d5c1-57e8-6d6d-7ddc-5ab8ff9f28c4','aos_products_quotes_created_by','Users','users','id','AOS_Products_Quotes','aos_products_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('41bc7b7b-686a-eae3-fd6b-5ab8ff5afc8c','aobh_businesshours_modified_user','Users','users','id','AOBH_BusinessHours','aobh_businesshours','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('423fcffb-b215-fa6d-cffe-5ab8ff19eb85','aobh_businesshours_created_by','Users','users','id','AOBH_BusinessHours','aobh_businesshours','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('42e17215-5044-dcd6-77e9-5ab8ffc8ddd0','aos_products_quotes_assigned_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4359a535-0b37-d0d1-a67c-5ab8ff1243a0','aos_product_quotes_aos_products','AOS_Products','aos_products','id','AOS_Products_Quotes','aos_products_quotes','product_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('43e95e22-627b-a344-74c1-5ab8ff0fc994','bug_calls','Bugs','bugs','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),
	('44577d74-b8f6-32b2-3f19-5ab8ffd32ed9','bug_emails','Bugs','bugs','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),
	('44a4bff6-03d8-d62a-97b4-5ab8ffe999a1','sugarfeed_modified_user','Users','users','id','SugarFeed','sugarfeed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('45037acc-cffc-e096-63b7-5ab8ff33f020','bug_notes','Bugs','bugs','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),
	('454e7774-08ca-20b4-521d-5ab8ff80ddd1','sugarfeed_created_by','Users','users','id','SugarFeed','sugarfeed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('462c317d-356e-ae6d-616d-5ab8ff753bde','sugarfeed_assigned_user','Users','users','id','SugarFeed','sugarfeed','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('46b1dc5c-4eb7-5845-69a7-5ab8ff8c5025','bugs_release','Releases','releases','id','Bugs','bugs','found_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('46e50484-f2d2-b6b9-d7f8-5ab8ff9098da','aos_quotes_aos_invoices','AOS_Quotes','aos_quotes','id','AOS_Invoices','aos_invoices','id','aos_quotes_aos_invoices_c','aos_quotes77d9_quotes_ida','aos_quotes6b83nvoices_idb','many-to-many',NULL,NULL,0,0),
	('4719186a-379b-b22c-9fc2-5ab8ff1e30f1','aos_line_item_groups_modified_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('479a4bbf-f520-a7e2-8eaf-5ab8ffb94433','bugs_fixed_in_release','Releases','releases','id','Bugs','bugs','fixed_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('47ba8352-2513-55e7-854e-5ab8ff65e8d2','aos_line_item_groups_created_by','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('48410a60-00cb-8e98-4545-5ab8ff547b41','aos_line_item_groups_assigned_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('48834b09-a6bb-b797-c014-5ab8ff76f649','user_direct_reports','Users','users','id','Users','users','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('48b2546e-2921-dd5f-1d2f-5ab8ff777830','groups_aos_product_quotes','AOS_Line_Item_Groups','aos_line_item_groups','id','AOS_Products_Quotes','aos_products_quotes','group_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('490e3e7f-95eb-d1ef-957f-5ab8ffd7bbc8','users_email_addresses','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Users',0,0),
	('4c056cc8-7358-97cc-bd09-5ab8ffd855e4','users_email_addresses_primary','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),
	('4d7bc1b8-fc81-f396-8caa-5ab8ffae7014','eapm_modified_user','Users','users','id','EAPM','eapm','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4d80f189-5cc3-f039-04aa-5ab8ff2ab827','aos_quotes_modified_user','Users','users','id','AOS_Quotes','aos_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4de9a459-b55c-0980-6d34-5ab8ffd6b823','eapm_created_by','Users','users','id','EAPM','eapm','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4df70c61-34f0-4f32-e57a-5ab8ff40feff','aos_quotes_project','AOS_Quotes','aos_quotes','id','Project','project','id','aos_quotes_project_c','aos_quotes1112_quotes_ida','aos_quotes7207project_idb','many-to-many',NULL,NULL,0,0),
	('4e65fde6-57b6-66b4-719f-5ab8ff40ce96','aos_quotes_created_by','Users','users','id','AOS_Quotes','aos_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4e8b2e6e-871e-8e4f-726e-5ab8ffde1401','eapm_assigned_user','Users','users','id','EAPM','eapm','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4ed3add1-8d6b-09d9-6544-5ab8ff24a575','aos_quotes_assigned_user','Users','users','id','AOS_Quotes','aos_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4f42a60d-1112-c0c7-ef2e-5ab8ff45287d','securitygroups_aos_quotes','SecurityGroups','securitygroups','id','AOS_Quotes','aos_quotes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Quotes',0,0),
	('4fbefa58-bb73-de89-cf8a-5ab8fff5ced7','aos_quotes_aos_product_quotes','AOS_Quotes','aos_quotes','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('4fcfa781-1a19-435a-a392-5ab8ffa2681b','campaignlog_contact','CampaignLog','campaign_log','related_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('50153819-9de9-f0a9-4625-5ab8ff652bf5','aos_quotes_aos_line_item_groups','AOS_Quotes','aos_quotes','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('50520863-8fea-452a-1d36-5ab8ffccec7c','emails_contacts_rel','Emails','emails','id','Contacts','contacts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Contacts',0,0),
	('5066d9d8-a9d5-86f2-5d77-5ab8fffa19aa','campaignlog_lead','CampaignLog','campaign_log','related_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('50e06d24-5fae-2a56-ee60-5ab8ffd15e22','campaignlog_created_opportunities','CampaignLog','campaign_log','related_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5108bc27-3d5c-52b0-f8a2-5ab8ff35ffb3','oauthkeys_modified_user','Users','users','id','OAuthKeys','oauth_consumer','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5174cd0c-cdba-27a7-0f2b-5ab8ffe0448e','campaignlog_targeted_users','CampaignLog','campaign_log','target_id','Users','users','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('51806b27-7671-f9d1-d380-5ab8ff7dce0e','oauthkeys_created_by','Users','users','id','OAuthKeys','oauth_consumer','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('51d691af-e186-c20d-cccc-5ab8ff710c24','oauthkeys_assigned_user','Users','users','id','OAuthKeys','oauth_consumer','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('521bfe8a-524a-47d3-4a73-5ab8fffb303a','campaignlog_sent_emails','CampaignLog','campaign_log','related_id','Emails','emails','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('52c41250-e9ca-58c2-08ae-5ab8ffe1cc8e','aow_actions_modified_user','Users','users','id','AOW_Actions','aow_actions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('532e7ecc-4f35-deb6-0560-5ab8ffae761c','aow_actions_created_by','Users','users','id','AOW_Actions','aow_actions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('55788bc3-296e-8a4d-e047-5ab8ff3655a5','securitygroups_project','SecurityGroups','securitygroups','id','Project','project','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Project',0,0),
	('5606014b-55cf-48b1-8389-5ab8ff4c0efb','aow_processed_aow_actions','AOW_Processed','aow_processed','id','AOW_Actions','aow_actions','id','aow_processed_aow_actions','aow_processed_id','aow_action_id','many-to-many',NULL,NULL,0,0),
	('5654b5a2-794f-857b-59a8-5ab8ffc98a88','consumer_tokens','OAuthKeys','oauth_consumer','id','OAuthTokens','oauth_tokens','consumer',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('56744337-f873-f377-1306-5ab8ff6c641a','projects_notes','Project','project','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('572f9145-9d13-cc24-b5aa-5ab8fffa0677','aow_workflow_modified_user','Users','users','id','AOW_WorkFlow','aow_workflow','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5735cafd-a206-f1b1-c473-5ab8ff7f7449','projects_tasks','Project','project','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('579c838a-3d77-596a-39ae-5ab8ff358d37','aow_workflow_created_by','Users','users','id','AOW_WorkFlow','aow_workflow','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('57a358d3-62cc-c9ba-c70c-5ab8ff9e03f9','projects_meetings','Project','project','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('580b0f8d-c4a3-f98f-806e-5ab8ffa17381','oauthtokens_assigned_user','Users','users','id','OAuthTokens','oauth_tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('58168fe3-f09f-b9c0-a0b7-5ab8ffc03911','aow_workflow_assigned_user','Users','users','id','AOW_WorkFlow','aow_workflow','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('582ce6af-f0d2-f4e5-5dce-5ab8ffa8b6f9','projects_calls','Project','project','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('588f2048-8737-47fb-c865-5ab8ffa0170b','securitygroups_aow_workflow','SecurityGroups','securitygroups','id','AOW_WorkFlow','aow_workflow','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOW_WorkFlow',0,0),
	('5895fb09-ea2e-fa5e-90b6-5ab8ff2f9437','projects_emails','Project','project','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('591ed55e-ba3a-a192-5d1e-5ab8ff11b35f','aow_workflow_aow_conditions','AOW_WorkFlow','aow_workflow','id','AOW_Conditions','aow_conditions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('59228b80-ecc3-ce51-87ea-5ab8ff5194e2','projects_project_tasks','Project','project','id','ProjectTask','project_task','project_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('595704a5-75a2-275b-5339-5ab8ffdfd8b9','emails_accounts_rel','Emails','emails','id','Accounts','accounts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Accounts',0,0),
	('599fbfae-bd2e-c2ee-c4d1-5ab8ff770ffe','projects_assigned_user','Users','users','id','Project','project','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('59a71b24-1f90-0fc0-b8f0-5ab8ff1cf706','aow_workflow_aow_actions','AOW_WorkFlow','aow_workflow','id','AOW_Actions','aow_actions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5a17b85e-c554-d589-9d00-5ab8ff878d51','projects_modified_user','Users','users','id','Project','project','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5a20cdf6-19e9-ed96-5eaa-5ab8ff243489','aow_workflow_aow_processed','AOW_WorkFlow','aow_workflow','id','AOW_Processed','aow_processed','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5aac18eb-cb20-6cab-67e7-5ab8ff03c44a','am_projecttemplates_modified_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5ad80f15-5f8d-3816-ef61-5ab8ff41b9c8','fp_event_locations_fp_events_1','FP_Event_Locations','fp_event_locations','id','FP_events','fp_events','id','fp_event_locations_fp_events_1_c','fp_event_locations_fp_events_1fp_event_locations_ida','fp_event_locations_fp_events_1fp_events_idb','many-to-many',NULL,NULL,0,0),
	('5ae4bfca-8225-a59b-9fd1-5ab8ffdba6c5','projects_created_by','Users','users','id','Project','project','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5b193afa-72ba-b498-8362-5ab8ff430113','am_projecttemplates_created_by','Users','users','id','AM_ProjectTemplates','am_projecttemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5b935d65-3995-3f2d-80fe-5ab8ffe47f1b','am_projecttemplates_assigned_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5bfab13c-a96a-8aa4-3253-5ab8ff7362d1','aow_processed_modified_user','Users','users','id','AOW_Processed','aow_processed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5c9f467a-9c08-c3f4-b1a0-5ab8ff12b43c','aow_processed_created_by','Users','users','id','AOW_Processed','aow_processed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5e2994e9-af70-2785-5570-5ab8ff60f4de','am_tasktemplates_modified_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5e2f94a4-0c2a-bdb9-5451-5ab8ff8e321a','securitygroups_projecttask','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),
	('5ecce66f-a45a-c615-e3f7-5ab8ff165350','am_tasktemplates_created_by','Users','users','id','AM_TaskTemplates','am_tasktemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5ed03a33-4d7e-398e-6f3f-5ab8ff8d9f23','aow_conditions_modified_user','Users','users','id','AOW_Conditions','aow_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5f318722-eacd-622d-28a4-5ab8ff6204e7','project_tasks_notes','ProjectTask','project_task','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),
	('5f407894-722f-b7ea-d187-5ab8ff15ca8a','aow_conditions_created_by','Users','users','id','AOW_Conditions','aow_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('5f9aa899-5bcb-1e74-af37-5ab8ff4312fa','am_tasktemplates_assigned_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('60970fa1-0360-3424-d011-5ab8ffac5121','project_tasks_tasks','ProjectTask','project_task','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),
	('61100240-16fa-a381-fa0e-5ab8ff5939eb','project_tasks_meetings','ProjectTask','project_task','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),
	('61339acf-b02f-48da-ff21-5ab8ff87dd36','favorites_modified_user','Users','users','id','Favorites','favorites','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('61350592-6f1b-c111-22d2-5ab8ffe4d62b','emails_leads_rel','Emails','emails','id','Leads','leads','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Leads',0,0),
	('615c0fb9-5cd4-2ee8-9785-5ab8ff4db70d','fp_events_contacts','FP_events','fp_events','id','Contacts','contacts','id','fp_events_contacts_c','fp_events_contactsfp_events_ida','fp_events_contactscontacts_idb','many-to-many',NULL,NULL,0,0),
	('61779c05-3d74-799c-4db1-5ab8ff4a7886','favorites_created_by','Users','users','id','Favorites','favorites','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('61875128-ec38-7478-e443-5ab8ff2f6fd6','project_tasks_calls','ProjectTask','project_task','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),
	('61eaa9c6-72e9-4dc7-4cab-5ab8fff3fe3f','favorites_assigned_user','Users','users','id','Favorites','favorites','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('61ef9e0a-4743-604c-7ee0-5ab8ffc76def','project_tasks_emails','ProjectTask','project_task','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),
	('62a02e34-2e4d-677d-57f1-5ab8ffcd7e6f','project_tasks_assigned_user','Users','users','id','ProjectTask','project_task','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('64b5db8f-0d8f-8cae-7fb6-5ab8ff1b2ef0','aok_knowledge_base_categories_modified_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('64d8b8d4-b483-6468-57e8-5ab8ff81db54','jjwg_maps_modified_user','Users','users','id','jjwg_Maps','jjwg_maps','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6529caa3-33b8-7d8a-6dc6-5ab8ff50dac9','aok_knowledge_base_categories_created_by','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('654142cf-b3eb-837c-0a0e-5ab8ff10db65','jjwg_maps_created_by','Users','users','id','jjwg_Maps','jjwg_maps','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('659be907-ee99-7d90-b039-5ab8ff0b2b03','aok_knowledge_base_categories_assigned_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('65c10269-b8b1-6965-93ad-5ab8ff16e33c','jjwg_maps_assigned_user','Users','users','id','jjwg_Maps','jjwg_maps','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6647e95c-2f2b-7eb4-86b7-5ab8ffc1cf5d','securitygroups_jjwg_maps','SecurityGroups','securitygroups','id','jjwg_Maps','jjwg_maps','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Maps',0,0),
	('66ea3873-07b2-1c73-dd3a-5ab8ff228445','jjwg_Maps_accounts','jjwg_Maps','jjwg_Maps','parent_id','Accounts','accounts','id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('6798bee2-f36e-e5af-48e5-5ab8ff345ff5','jjwg_Maps_contacts','jjwg_Maps','jjwg_Maps','parent_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),
	('6806d1fb-03f4-6b7e-a1c3-5ab8ff52cee2','jjwg_Maps_leads','jjwg_Maps','jjwg_Maps','parent_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),
	('68526675-67d3-7ba2-79e0-5ab8ff8fc557','aok_knowledgebase_modified_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('68b499c8-7c38-8147-13a2-5ab8ffd06aa5','jjwg_Maps_opportunities','jjwg_Maps','jjwg_Maps','parent_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('68f7679f-5c88-e7f2-d704-5ab8ff64e4ca','aok_knowledgebase_created_by','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('69050734-1d58-011c-8922-5ab8ffb115fd','fp_events_fp_event_locations_1','FP_events','fp_events','id','FP_Event_Locations','fp_event_locations','id','fp_events_fp_event_locations_1_c','fp_events_fp_event_locations_1fp_events_ida','fp_events_fp_event_locations_1fp_event_locations_idb','many-to-many',NULL,NULL,0,0),
	('6916032c-b619-bc31-90eb-5ab8ff7cf5b6','emails_aos_contracts_rel','Emails','emails','id','AOS_Contracts','aos_contracts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','AOS_Contracts',0,0),
	('696b5829-b98f-f689-0ffa-5ab8ff4618c4','jjwg_Maps_cases','jjwg_Maps','jjwg_Maps','parent_id','Cases','cases','id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),
	('6973dec9-f392-b001-0c6f-5ab8ff163277','aok_knowledgebase_assigned_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('69eb189b-889b-0034-9676-5ab8ff538641','jjwg_Maps_projects','jjwg_Maps','jjwg_Maps','parent_id','Project','project','id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),
	('69efdb0a-0044-a8f2-8bf3-5ab8ffe978e2','securitygroups_aok_knowledgebase','SecurityGroups','securitygroups','id','AOK_KnowledgeBase','aok_knowledgebase','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOK_KnowledgeBase',0,0),
	('6a895492-b813-7fb4-6d99-5ab8ff541549','jjwg_Maps_meetings','jjwg_Maps','jjwg_Maps','parent_id','Meetings','meetings','id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),
	('6c98a593-dbfb-9072-5124-5ab8ff144039','project_tasks_modified_user','Users','users','id','ProjectTask','project_task','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6cb03181-5b74-412a-ee95-5ab8ff837785','reminders_modified_user','Users','users','id','Reminders','reminders','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6d034ff8-1714-bbaf-127d-5ab8ff4f4697','project_tasks_created_by','Users','users','id','ProjectTask','project_task','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6d137e20-714f-f3de-726d-5ab8ffceab67','reminders_created_by','Users','users','id','Reminders','reminders','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6d7f0028-5088-fee7-a3e4-5ab8ffc164aa','reminders_assigned_user','Users','users','id','Reminders','reminders','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('6f6305f3-989e-4b3f-caaa-5ab8ff887d59','fp_events_leads_1','FP_events','fp_events','id','Leads','leads','id','fp_events_leads_1_c','fp_events_leads_1fp_events_ida','fp_events_leads_1leads_idb','many-to-many',NULL,NULL,0,0),
	('701c0bd8-9de1-939b-2e3e-5ab8ffd82ca9','emails_meetings_rel','Emails','emails','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('7114a9c4-18ce-4d65-717e-5ab8ffcd331f','campaigns_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('717c5991-4651-ecd7-3337-5ab8ff10cb98','campaigns_created_by','Users','users','id','Campaigns','campaigns','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('71f7d32c-e297-eca6-7fa4-5ab8fffe888f','campaigns_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('72b0be0b-5b9a-6f74-9e9a-5ab8ffea2900','securitygroups_campaigns','SecurityGroups','securitygroups','id','Campaigns','campaigns','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Campaigns',0,0),
	('733b140e-c1ea-f0fe-4b05-5ab8ff776aa2','campaign_accounts','Campaigns','campaigns','id','Accounts','accounts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('73a37a11-d556-1119-bc1b-5ab8ffe7208d','campaign_contacts','Campaigns','campaigns','id','Contacts','contacts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('749940ca-6195-9fa2-ff91-5ab8ff2adac3','campaign_leads','Campaigns','campaigns','id','Leads','leads','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('751d8b0e-be99-332b-1b8f-5ab8ff251cc2','campaign_prospects','Campaigns','campaigns','id','Prospects','prospects','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('758ef33c-f5cc-f310-d643-5ab8ffa3947f','campaign_opportunities','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('75f5c2a6-81ad-b7cc-227a-5ab8fff7e9ff','campaign_email_marketing','Campaigns','campaigns','id','EmailMarketing','email_marketing','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('7736cb94-d149-e6f0-004a-5ab8ff2b9450','campaign_emailman','Campaigns','campaigns','id','EmailMan','emailman','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('7773001f-1fc0-3c8b-b034-5ab8ff70a486','fp_events_prospects_1','FP_events','fp_events','id','Prospects','prospects','id','fp_events_prospects_1_c','fp_events_prospects_1fp_events_ida','fp_events_prospects_1prospects_idb','many-to-many',NULL,NULL,0,0),
	('77b0ed51-6f30-44e0-1ed3-5ab8ff573ecd','campaign_campaignlog','Campaigns','campaigns','id','CampaignLog','campaign_log','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('78b775f6-2ded-36cd-5224-5ab8ff57f37d','campaign_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('78d5e27d-f108-7c82-b707-5ab8ff0a1b44','jjwg_Maps_prospects','jjwg_Maps','jjwg_Maps','parent_id','Prospects','prospects','id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('79348f59-6506-b032-b4b3-5ab8ff95c1af','campaign_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('799c2923-79ef-56f9-4351-5ab8ffbda78f','surveyresponses_campaigns','Campaigns','campaigns','id','SurveyResponses','surveyresponses','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('7c2f57cc-51e7-6814-5a43-5ab8fffdbc6e','prospectlists_assigned_user','Users','users','id','prospectlists','prospect_lists','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('7cd2d6b6-3cf2-87e8-bd73-5ab8ffd83f7c','securitygroups_prospectlists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),
	('7d940daf-b1ab-6c11-8d1f-5ab8ffa124b6','jjwg_maps_jjwg_areas','jjwg_Maps','jjwg_maps','id','jjwg_Areas','jjwg_areas','id','jjwg_maps_jjwg_areas_c','jjwg_maps_5304wg_maps_ida','jjwg_maps_41f2g_areas_idb','many-to-many',NULL,NULL,0,0),
	('80ccfcc3-9bda-5ac5-4c6d-5ab8ff46dbee','prospects_modified_user','Users','users','id','Prospects','prospects','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8177842a-d741-fa0c-83c6-5ab8ff2ecb4e','prospects_created_by','Users','users','id','Prospects','prospects','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('81ed473d-4f22-8ccd-7ee3-5ab8ff69551b','prospects_assigned_user','Users','users','id','Prospects','prospects','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('822f3f75-cef5-38a2-416f-5ab8ff53c36c','jjwg_markers_modified_user','Users','users','id','jjwg_Markers','jjwg_markers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('824a1ab8-f618-e23c-49fa-5ab8ff711e06','securitygroups_prospects','SecurityGroups','securitygroups','id','Prospects','prospects','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Prospects',0,0),
	('82fa3b01-1aa1-6776-e5a6-5ab8ffe39326','prospects_email_addresses','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Prospects',0,0),
	('836474ab-9df3-0296-799c-5ab8ff9272a0','jjwg_markers_created_by','Users','users','id','jjwg_Markers','jjwg_markers','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8389bba5-546d-dec6-1541-5ab8ff5ec66a','prospects_email_addresses_primary','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),
	('83f349a9-f0b4-f957-b94f-5ab8ffff39dc','prospect_tasks','Prospects','prospects','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('842f096b-fa80-1d88-eea7-5ab8fffd6c73','jjwg_markers_assigned_user','Users','users','id','jjwg_Markers','jjwg_markers','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8466acd2-9560-6eea-0c5c-5ab8ffe77aa9','prospect_notes','Prospects','prospects','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('84910162-c0b9-7247-39e4-5ab8ff1d1b85','jjwg_maps_jjwg_markers','jjwg_Maps','jjwg_maps','id','jjwg_Markers','jjwg_markers','id','jjwg_maps_jjwg_markers_c','jjwg_maps_b229wg_maps_ida','jjwg_maps_2e31markers_idb','many-to-many',NULL,NULL,0,0),
	('84fbda80-d746-0522-4ffd-5ab8ffe2afd8','prospect_meetings','Prospects','prospects','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('85648968-66b2-5c04-59b8-5ab8ffb513f5','securitygroups_jjwg_markers','SecurityGroups','securitygroups','id','jjwg_Markers','jjwg_markers','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Markers',0,0),
	('8596c67a-fd84-28f3-f1bb-5ab8ff0d5921','prospect_calls','Prospects','prospects','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('8613636a-bd4d-6713-2bc7-5ab8ff742bb9','prospect_emails','Prospects','prospects','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),
	('866ecc18-4c24-de5d-c944-5ab8fffe58ea','prospect_campaign_log','Prospects','prospects','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Prospects',0,0),
	('88800051-c52a-eccb-84ef-5ab8ffb7073f','documents_bugs','Documents','documents','id','Bugs','bugs','id','documents_bugs','document_id','bug_id','many-to-many',NULL,NULL,0,0),
	('892aaded-bf20-9ca7-0e4c-5ab8ffe10c59','email_template_email_marketings','EmailTemplates','email_templates','id','EmailMarketing','email_marketing','template_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8a916b25-7cb1-24df-a656-5ab8ff28ce53','campaign_campaigntrakers','Campaigns','campaigns','id','CampaignTrackers','campaign_trkrs','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8b6f0341-c6d9-47f6-9341-5ab8ff3f1012','project_contacts_1','Project','project','id','Contacts','contacts','id','project_contacts_1_c','project_contacts_1project_ida','project_contacts_1contacts_idb','many-to-many',NULL,NULL,0,0),
	('8dcbe28d-642c-e3d3-a5c2-5ab8ff3ec547','reminders_invitees_modified_user','Users','users','id','Reminders_Invitees','reminders_invitees','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8e8f0065-1e7d-b453-9128-5ab8ffcb1f6e','jjwg_areas_modified_user','Users','users','id','jjwg_Areas','jjwg_areas','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8f564223-288f-65f1-96c6-5ab8ff9489b6','reminders_invitees_created_by','Users','users','id','Reminders_Invitees','reminders_invitees','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('8fc1a542-3626-23e3-7b18-5ab8ff688ce4','jjwg_areas_created_by','Users','users','id','jjwg_Areas','jjwg_areas','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('90184968-27b5-e6b1-0ec4-5ab8fff12178','reminders_invitees_assigned_user','Users','users','id','Reminders_Invitees','reminders_invitees','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9035bbf6-98c9-041d-2e68-5ab8ffdeaf96','jjwg_areas_assigned_user','Users','users','id','jjwg_Areas','jjwg_areas','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9106ccbb-c9a9-d02e-be26-5ab8ffd5d8a4','schedulers_created_by_rel','Users','users','id','Schedulers','schedulers','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),
	('91493e21-d497-78b6-fdc2-5ab8ffd9dec6','securitygroups_jjwg_areas','SecurityGroups','securitygroups','id','jjwg_Areas','jjwg_areas','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Areas',0,0),
	('91a51b16-2cc6-5468-d1f5-5ab8ff8ddd5c','schedulers_modified_user_id_rel','Users','users','id','Schedulers','schedulers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('922c25a3-440f-e001-1218-5ab8ff3d997f','schedulers_jobs_rel','Schedulers','schedulers','id','SchedulersJobs','job_queue','scheduler_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('92b7188d-ec24-3870-dba2-5ab8ffb808a2','fp_events_modified_user','Users','users','id','FP_events','fp_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('92ea0df8-a027-cb34-06b2-5ab8ff18c001','schedulersjobs_assigned_user','Users','users','id','SchedulersJobs','job_queue','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('937da04b-1904-a56b-1b80-5ab8ff6a7b32','fp_events_created_by','Users','users','id','FP_events','fp_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('93c003e8-b465-dacb-a6ce-5ab8ff8e2829','project_users_1','Project','project','id','Users','users','id','project_users_1_c','project_users_1project_ida','project_users_1users_idb','many-to-many',NULL,NULL,0,0),
	('94135fbf-ac0e-994a-2547-5ab8ff8ecf56','jjwg_address_cache_modified_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('944f8c08-a5ba-b61c-eda7-5ab8ff46df38','fp_events_assigned_user','Users','users','id','FP_events','fp_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('94846b68-79fd-b63c-88d1-5ab8ffba2bf5','jjwg_address_cache_created_by','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('949a776e-e930-7771-42bf-5ab8ff5265c2','securitygroups_fp_events','SecurityGroups','securitygroups','id','FP_events','fp_events','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_events',0,0),
	('95e724ad-9ede-1ca5-6b79-5ab8ff065cf6','jjwg_address_cache_assigned_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9724e908-0fbe-1dc1-66b8-5ab8ff99dc9e','fp_event_locations_modified_user','Users','users','id','FP_Event_Locations','fp_event_locations','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('97f434a4-4306-25c5-72e0-5ab8ff6a6e4a','fp_event_locations_created_by','Users','users','id','FP_Event_Locations','fp_event_locations','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('98660ec2-8726-f856-f37d-5ab8ffd66707','calls_reschedule_modified_user','Users','users','id','Calls_Reschedule','calls_reschedule','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('988af5e6-beae-04f9-c27e-5ab8ff632bce','fp_event_locations_assigned_user','Users','users','id','FP_Event_Locations','fp_event_locations','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('98c9708e-8b96-6128-86dc-5ab8ff503d36','calls_reschedule_created_by','Users','users','id','Calls_Reschedule','calls_reschedule','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('99014e02-042e-cd01-01e9-5ab8ff43b54c','securitygroups_fp_event_locations','SecurityGroups','securitygroups','id','FP_Event_Locations','fp_event_locations','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_Event_Locations',0,0),
	('996005f4-20b9-9865-a0a4-5ab8ff9d5882','calls_reschedule','Calls','calls','id','Calls_Reschedule','calls_reschedule','call_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('996f60d5-959f-9338-2ee5-5ab8ff0d390b','optimistic_locking',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),
	('99c8302d-b047-03df-bfbe-5ab8ff7f44dd','calls_reschedule_assigned_user','Users','users','id','Calls_Reschedule','calls_reschedule','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('99e7c7cf-4a86-d3be-925b-5ab8ff2c4bf2','unified_search',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),
	('9a0ac5de-9292-c7cb-0c62-5ab8ff3d311a','contacts_modified_user','Users','users','id','Contacts','contacts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9ad18b2f-c003-3909-fb8e-5ab8ffdb86ac','contacts_created_by','Users','users','id','Contacts','contacts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9bb3007b-f526-9de0-72e6-5ab8ffa5b02a','securitygroups_acl_roles','SecurityGroups','securitygroups','id','ACLRoles','acl_roles','id','securitygroups_acl_roles','securitygroup_id','role_id','many-to-many',NULL,NULL,0,0),
	('9c5781c4-4165-f8aa-bf5c-5ab8ff1c32ab','aod_indexevent_modified_user','Users','users','id','AOD_IndexEvent','aod_indexevent','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9cc1086a-a4b0-3ae0-6085-5ab8ff2b53f0','aod_indexevent_created_by','Users','users','id','AOD_IndexEvent','aod_indexevent','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9d268703-97c3-edb9-fe8f-5ab8ff6353c1','aod_indexevent_assigned_user','Users','users','id','AOD_IndexEvent','aod_indexevent','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9dbdca3c-b17b-8745-e616-5ab8ff789983','securitygroups_modified_user','Users','users','id','SecurityGroups','securitygroups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9e4326d9-9535-e37b-3614-5ab8ff423add','securitygroups_created_by','Users','users','id','SecurityGroups','securitygroups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('9ee855a2-38ac-8c28-7a4a-5ab8ff814fc2','securitygroups_assigned_user','Users','users','id','SecurityGroups','securitygroups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a116fc8c-93f3-7c59-312b-5ab8ffd178ff','outbound_email_modified_user','Users','users','id','OutboundEmailAccounts','outbound_email','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a1997a03-eb81-e894-b4ef-5ab8ff95d28d','outbound_email_created_by','Users','users','id','OutboundEmailAccounts','outbound_email','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a20b5eff-87b9-bc04-7016-5ab8ff53227d','outbound_email_assigned_user','Users','users','id','OutboundEmailAccounts','outbound_email','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a3e795fa-9e2c-469b-f583-5ab8ff558205','templatesectionline_modified_user','Users','users','id','TemplateSectionLine','templatesectionline','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a45c5a15-503e-a7b2-2523-5ab8ff25c220','templatesectionline_created_by','Users','users','id','TemplateSectionLine','templatesectionline','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a847ccb6-88eb-c8ef-ef06-5ab8ff51f027','oauth2tokens_modified_user','Users','users','id','OAuth2Tokens','oauth2tokens','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a8fe4f02-a0ac-8867-20f9-5ab8ff1b0e8b','oauth2tokens_created_by','Users','users','id','OAuth2Tokens','oauth2tokens','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a957e610-2300-52be-8666-5ab8ff0e610e','aod_index_modified_user','Users','users','id','AOD_Index','aod_index','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a9bd431a-814c-8cf3-f73a-5ab8ff92b9f3','oauth2tokens_assigned_user','Users','users','id','OAuth2Tokens','oauth2tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a9e9c6ce-536e-bf58-5990-5ab8ff5323fa','aod_index_created_by','Users','users','id','AOD_Index','aod_index','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('a9fa68f2-4bd8-d604-1c62-5ab8ffc3975b','contacts_assigned_user','Users','users','id','Contacts','contacts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('aa8834c5-482a-a80f-b1d5-5ab8ff5db17a','securitygroups_contacts','SecurityGroups','securitygroups','id','Contacts','contacts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Contacts',0,0),
	('abb1f553-9e9d-bd33-6b12-5ab8ff0abf0c','contacts_email_addresses','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Contacts',0,0),
	('ac6b84a2-e4ce-36cd-416c-5ab8ff47b505','contacts_email_addresses_primary','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),
	('ace7848f-2783-2479-450a-5ab8ff0df462','contact_direct_reports','Contacts','contacts','id','Contacts','contacts','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ad60e561-5c5a-3125-6ccc-5ab8ff6281b1','contact_leads','Contacts','contacts','id','Leads','leads','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('adf057df-af32-cd73-1a2a-5ab8ffcefda6','contact_notes','Contacts','contacts','id','Notes','notes','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ae5e0b50-86c4-85a7-7c52-5ab8ff1b2126','contact_tasks','Contacts','contacts','id','Tasks','tasks','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('aec4e7e1-d489-8800-0ab3-5ab8ff26d352','contact_tasks_parent','Contacts','contacts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),
	('af57d6bb-ac6b-17f0-c360-5ab8ffbded80','contact_notes_parent','Contacts','contacts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),
	('afb73de5-c4d6-9632-cf96-5ab8ff188320','contact_campaign_log','Contacts','contacts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Contacts',0,0),
	('b016044d-beab-de92-20a2-5ab8ff93107e','securitygroups_project_task','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),
	('b0258a6a-ddab-327a-4932-5ab8ff70a51b','contact_aos_quotes','Contacts','contacts','id','AOS_Quotes','aos_quotes','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b0640d6c-5d99-4081-b93f-5ab8ff57f6e9','meetings_modified_user','Users','users','id','Meetings','meetings','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b09b9725-4e7e-a1de-8a4c-5ab8ff1f2ddc','contact_aos_invoices','Contacts','contacts','id','AOS_Invoices','aos_invoices','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b107fce3-b794-088e-9f78-5ab8ffab446d','contact_aos_contracts','Contacts','contacts','id','AOS_Contracts','aos_contracts','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b1eead99-dae4-42fc-3472-5ab8ffa58c68','contacts_aop_case_updates','Contacts','contacts','id','AOP_Case_Updates','aop_case_updates','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b6be0546-1d61-cee1-c974-5ab8ff0d7d56','meetings_created_by','Users','users','id','Meetings','meetings','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b7350d4c-971b-1da7-e950-5ab8ff159772','securitygroups_prospect_lists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),
	('b83040dd-ab4a-142a-3f36-5ab8ffdb914f','oauth2clients_modified_user','Users','users','id','OAuth2Clients','oauth2clients','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b8a2d44c-2c06-5b14-2302-5ab8ff7791db','oauth2clients_created_by','Users','users','id','OAuth2Clients','oauth2clients','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b90e704d-0e02-38cd-d57e-5ab8ff719c45','oauth2clients_oauth2tokens','OAuth2Clients','oauth2clients','id','OAuth2Tokens','oauth2tokens','client',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('b9ce9d47-d8ac-cd86-3b00-5ab8ff9bad77','oauth2clients_assigned_user','Users','users','id','OAuth2Clients','oauth2clients','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('baa5220d-a3ac-e074-167c-5ab8ffc56735','aod_index_assigned_user','Users','users','id','AOD_Index','aod_index','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('baf1b06d-1ee5-a2ca-93f5-5ab8ff3122f7','accounts_modified_user','Users','users','id','Accounts','accounts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('bb70ad1e-03f4-3eb2-09bf-5ab8ff552dd9','accounts_created_by','Users','users','id','Accounts','accounts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('be46194c-f54f-4ac5-f620-5ab8ff0a30a6','surveyresponses_modified_user','Users','users','id','SurveyResponses','surveyresponses','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('be530073-a124-4556-66a5-5ab8ff94ec3f','meetings_assigned_user','Users','users','id','Meetings','meetings','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('beb3ecc4-88c6-a959-7b01-5ab8ff9d60b9','surveyresponses_created_by','Users','users','id','SurveyResponses','surveyresponses','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('bed1b4c2-d9e7-a1ac-c311-5ab8fffded2a','aop_case_events_modified_user','Users','users','id','AOP_Case_Events','aop_case_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('bf9f1b75-1135-a258-6e0f-5ab8ff6fe5a3','surveyresponses_assigned_user','Users','users','id','SurveyResponses','surveyresponses','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c01c08db-b11d-9c38-2f20-5ab8ffdf28be','securitygroups_surveyresponses','SecurityGroups','securitygroups','id','SurveyResponses','surveyresponses','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyResponses',0,0),
	('c099631a-986b-b4cb-1339-5ab8ff1631b9','surveyresponses_surveyquestionresponses','SurveyResponses','surveyresponses','id','SurveyQuestionResponses','surveyquestionresponses','surveyresponse_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c0c192c6-8a8f-9c16-2273-5ab8ff70664e','accounts_assigned_user','Users','users','id','Accounts','accounts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c1797c13-6c6e-bbfe-4cf3-5ab8ffd5e70d','securitygroups_accounts','SecurityGroups','securitygroups','id','Accounts','accounts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Accounts',0,0),
	('c18815b0-3e31-5093-607d-5ab8ff38abd4','aop_case_events_created_by','Users','users','id','AOP_Case_Events','aop_case_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c2c200f3-7c1e-e318-eee8-5ab8ff4de26b','accounts_email_addresses','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Accounts',0,0),
	('c34a0661-aee7-2bab-661a-5ab8ff898cd4','securitygroups_users','SecurityGroups','securitygroups','id','Users','users','id','securitygroups_users','securitygroup_id','user_id','many-to-many',NULL,NULL,0,0),
	('c3514e85-4e27-5472-ac30-5ab8ff87a381','accounts_email_addresses_primary','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),
	('c4251df9-67df-a53f-817f-5ab8ffcc354b','surveys_modified_user','Users','users','id','Surveys','surveys','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c4e997e5-0ff3-bf58-b494-5ab8ff23d478','aop_case_events_assigned_user','Users','users','id','AOP_Case_Events','aop_case_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c517f3c9-4b5b-91e5-b536-5ab8ff82d425','surveys_created_by','Users','users','id','Surveys','surveys','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c558661d-2fa2-b9b7-2321-5ab8ffee6be1','member_accounts','Accounts','accounts','id','Accounts','accounts','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c58640c6-3581-16f4-d712-5ab8ffb09822','surveys_assigned_user','Users','users','id','Surveys','surveys','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c5d871de-6327-27b6-3006-5ab8ff5f227a','cases_aop_case_events','Cases','cases','id','AOP_Case_Events','aop_case_events','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c5ee6ef9-de76-59ca-f3b5-5ab8ff417219','securitygroups_surveys','SecurityGroups','securitygroups','id','Surveys','surveys','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Surveys',0,0),
	('c63973cd-de56-ca29-f50f-5ab8ffdf582b','account_cases','Accounts','accounts','id','Cases','cases','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c659fee8-0d42-1dfd-10a7-5ab8ff05c1c1','surveys_surveyquestions','Surveys','surveys','id','SurveyQuestions','surveyquestions','survey_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c67e0d0f-96b5-0eb3-c835-5ab8ff249011','securitygroups_meetings','SecurityGroups','securitygroups','id','Meetings','meetings','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Meetings',0,0),
	('c6b0718b-bb9a-f022-38e7-5ab8ff94229f','account_tasks','Accounts','accounts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('c6cd66ab-8af8-6a79-6a55-5ab8ffd1a2db','surveys_surveyresponses','Surveys','surveys','id','SurveyResponses','surveyresponses','survey_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c72489ff-f95a-e7c4-44a0-5ab8ff52c723','account_notes','Accounts','accounts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('c819e319-2bdc-38ee-f5b8-5ab8fff03834','account_meetings','Accounts','accounts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('c892fc28-ff95-dfbb-76c7-5ab8ffa01442','account_calls','Accounts','accounts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('c8d02162-89bb-d7ad-db32-5ab8ff73b0ea','surveyquestionresponses_modified_user','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c8f9fc29-f5fa-135a-7595-5ab8ffa955b3','account_emails','Accounts','accounts','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),
	('c93be96f-079f-fed9-fc37-5ab8ff75f765','surveyquestionresponses_created_by','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c963f175-bb64-62c7-39ec-5ab8ff262bb4','account_leads','Accounts','accounts','id','Leads','leads','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('c9a2967c-6ae5-3349-d018-5ab8ff48714b','surveyquestionresponses_assigned_user','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ca1f7f48-af69-7414-07d0-5ab8ff56fb21','securitygroups_surveyquestionresponses','SecurityGroups','securitygroups','id','SurveyQuestionResponses','surveyquestionresponses','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestionResponses',0,0),
	('ca47e699-895a-f342-07df-5ab8ff1091af','account_campaign_log','Accounts','accounts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Accounts',0,0),
	('cabe62c6-4d0e-ede9-79d9-5ab8ff799408','account_aos_quotes','Accounts','accounts','id','AOS_Quotes','aos_quotes','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cb3fe87c-28c5-3b4a-6349-5ab8ffae0ff8','account_aos_invoices','Accounts','accounts','id','AOS_Invoices','aos_invoices','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cba9303e-afa9-3cb2-726e-5ab8ff6c3f76','account_aos_contracts','Accounts','accounts','id','AOS_Contracts','aos_contracts','contract_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cc46d7f3-4080-1e31-83bd-5ab8ffb71188','surveyquestions_modified_user','Users','users','id','SurveyQuestions','surveyquestions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ccc86786-b843-e495-875d-5ab8ffe1a272','surveyquestions_created_by','Users','users','id','SurveyQuestions','surveyquestions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cd450a08-a74f-a130-2628-5ab8ffb8a05c','surveyquestionoptions_surveyquestionresponses','SurveyQuestionOptions','surveyquestionoptions','id','SurveyQuestionResponses','surveyquestionresponses','id','surveyquestionoptions_surveyquestionresponses','surveyq72c7options_ida','surveyq10d4sponses_idb','many-to-many',NULL,NULL,0,0),
	('cd654c7d-547e-13da-ebe9-5ab8ff314333','surveyquestions_assigned_user','Users','users','id','SurveyQuestions','surveyquestions','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cd971acb-c911-4359-e55d-5ab8ffb863f9','aop_case_updates_modified_user','Users','users','id','AOP_Case_Updates','aop_case_updates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ce154971-3061-cde7-a758-5ab8ff2dc2c0','aop_case_updates_created_by','Users','users','id','AOP_Case_Updates','aop_case_updates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ce16838b-8972-21fb-d0a0-5ab8fff5b146','securitygroups_surveyquestions','SecurityGroups','securitygroups','id','SurveyQuestions','surveyquestions','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestions',0,0),
	('ce7c7982-2cc7-0913-1fcb-5ab8ffed8a7f','aop_case_updates_assigned_user','Users','users','id','AOP_Case_Updates','aop_case_updates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ce8ae838-d740-8b90-582b-5ab8ff954a98','surveyquestions_surveyquestionoptions','SurveyQuestions','surveyquestions','id','SurveyQuestionOptions','surveyquestionoptions','survey_question_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cee4fe4b-05c9-8bc4-2f05-5ab8ffba3861','cases_aop_case_updates','Cases','cases','id','AOP_Case_Updates','aop_case_updates','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('cf5e12b5-27c6-6cb2-97e3-5ab8ff81dc8e','aop_case_updates_notes','AOP_Case_Updates','aop_case_updates','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOP_Case_Updates',0,0),
	('d24c0e04-548c-5231-17d7-5ab8ff3b3dd1','surveyquestionoptions_modified_user','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d2ca8fad-7af2-74a1-bed4-5ab8ffa534cf','opportunities_modified_user','Users','users','id','Opportunities','opportunities','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d2ce9988-d53f-f3af-a267-5ab8ff5b01c3','surveyquestionoptions_created_by','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d33bd46e-a7a6-a72f-e2fe-5ab8ff78416d','surveyquestionoptions_assigned_user','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d349f17e-caba-0284-ce89-5ab8ff33067f','opportunities_created_by','Users','users','id','Opportunities','opportunities','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d3d89133-ce49-06cd-7e88-5ab8ff2ee799','securitygroups_surveyquestionoptions','SecurityGroups','securitygroups','id','SurveyQuestionOptions','surveyquestionoptions','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestionOptions',0,0),
	('d48aa031-9ce7-c71a-3e57-5ab8ff9fefb7','opportunities_assigned_user','Users','users','id','Opportunities','opportunities','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d4be44d1-1af6-ece3-6ef3-5ab8ff7bf079','aor_reports_modified_user','Users','users','id','AOR_Reports','aor_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d549975a-dedd-9fb4-3f8c-5ab8fff624cc','securitygroups_opportunities','SecurityGroups','securitygroups','id','Opportunities','opportunities','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Opportunities',0,0),
	('d5723a87-50f1-4942-809f-5ab8ffccc93c','aor_reports_created_by','Users','users','id','AOR_Reports','aor_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d62985cf-e025-67c1-58e9-5ab8ff66b8c6','aor_reports_assigned_user','Users','users','id','AOR_Reports','aor_reports','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d649b524-ef4c-5caf-ae5a-5ab8ff9dac2f','opportunity_calls','Opportunities','opportunities','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('d67e0e5c-3319-cf3a-0a7b-5ab8ffd208a5','meetings_notes','Meetings','meetings','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),
	('d6a42640-b2af-bff6-a1f8-5ab8ffc82071','securitygroups_aor_reports','SecurityGroups','securitygroups','id','AOR_Reports','aor_reports','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOR_Reports',0,0),
	('d6e32ec6-a030-fdd9-6769-5ab8ff1758b9','opportunity_meetings','Opportunities','opportunities','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('d71a23ec-78c0-8af9-4811-5ab8ff81f3a5','aor_reports_aor_fields','AOR_Reports','aor_reports','id','AOR_Fields','aor_fields','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d75ba925-0926-6c7f-90fc-5ab8ff68f0ed','accounts_bugs','Accounts','accounts','id','Bugs','bugs','id','accounts_bugs','account_id','bug_id','many-to-many',NULL,NULL,0,0),
	('d77333f0-2898-40a8-4a98-5ab8ff159c2f','opportunity_tasks','Opportunities','opportunities','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('d7856a06-c076-c25d-7c5a-5ab8ffcf6b48','aor_reports_aor_conditions','AOR_Reports','aor_reports','id','AOR_Conditions','aor_conditions','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d7cc82f3-1c9c-311e-8474-5ab8ff47e0a7','accounts_contacts','Accounts','accounts','id','Contacts','contacts','id','accounts_contacts','account_id','contact_id','many-to-many',NULL,NULL,0,0),
	('d7ed5d2d-7fe2-f539-ec5a-5ab8ffa01e6d','opportunity_notes','Opportunities','opportunities','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('d821bc32-a090-b736-5914-5ab8ff8e3f77','aor_scheduled_reports_aor_reports','AOR_Reports','aor_reports','id','AOR_Scheduled_Reports','aor_scheduled_reports','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d8553524-3b18-f8d8-6c38-5ab8ff0b73da','accounts_opportunities','Accounts','accounts','id','Opportunities','opportunities','id','accounts_opportunities','account_id','opportunity_id','many-to-many',NULL,NULL,0,0),
	('d8931559-f213-4223-d0a5-5ab8ff7dae31','opportunity_emails','Opportunities','opportunities','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),
	('d8cf47ae-629a-4dcb-48df-5ab8ffbaf57f','calls_contacts','Calls','calls','id','Contacts','contacts','id','calls_contacts','call_id','contact_id','many-to-many',NULL,NULL,0,0),
	('d9109931-85e6-e1ba-5406-5ab8ffc7e779','opportunity_leads','Opportunities','opportunities','id','Leads','leads','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d93235f6-a134-c7bc-3b43-5ab8ff5c7654','calls_users','Calls','calls','id','Users','users','id','calls_users','call_id','user_id','many-to-many',NULL,NULL,0,0),
	('d99133a3-0356-61b7-947f-5ab8fff76698','calls_leads','Calls','calls','id','Leads','leads','id','calls_leads','call_id','lead_id','many-to-many',NULL,NULL,0,0),
	('d9d263f7-e230-3d1d-aed4-5ab8ffff73eb','opportunity_currencies','Opportunities','opportunities','currency_id','Currencies','currencies','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('d9f40b61-355c-eb9b-46d8-5ab8ff2ebc8c','cases_bugs','Cases','cases','id','Bugs','bugs','id','cases_bugs','case_id','bug_id','many-to-many',NULL,NULL,0,0),
	('da64407d-8c2e-445c-66bc-5ab8ff939ed7','contacts_bugs','Contacts','contacts','id','Bugs','bugs','id','contacts_bugs','contact_id','bug_id','many-to-many',NULL,NULL,0,0),
	('dae0bf64-3730-9e15-64c1-5ab8ffd473cd','contacts_cases','Contacts','contacts','id','Cases','cases','id','contacts_cases','contact_id','case_id','many-to-many',NULL,NULL,0,0),
	('db1c097f-0279-6c46-39c3-5ab8ffd4adf7','aor_fields_modified_user','Users','users','id','AOR_Fields','aor_fields','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('db26e027-864b-4474-98ff-5ab8ff68e8ef','opportunities_campaign','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('db4bf55b-d22b-e2e9-09fe-5ab8fff24c9c','contacts_users','Contacts','contacts','id','Users','users','id','contacts_users','contact_id','user_id','many-to-many',NULL,NULL,0,0),
	('dba94f8b-6e5c-9cb0-9811-5ab8ff973a7e','aor_fields_created_by','Users','users','id','AOR_Fields','aor_fields','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('dbaad2e3-8de5-726b-31b7-5ab8ff41ccfa','opportunity_aos_quotes','Opportunities','opportunities','id','AOS_Quotes','aos_quotes','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('dbec7d35-9c56-7fd3-b99d-5ab8ffa85008','emails_bugs_rel','Emails','emails','id','Bugs','bugs','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Bugs',0,0),
	('dc758096-6746-8eec-d47c-5ab8ffe542ee','opportunity_aos_contracts','Opportunities','opportunities','id','AOS_Contracts','aos_contracts','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('dca4870e-0697-7c6a-614b-5ab8ff0912d1','emails_cases_rel','Emails','emails','id','Cases','cases','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Cases',0,0),
	('dd60e73b-0e37-0ba6-c7e4-5ab8ff47d812','emails_opportunities_rel','Emails','emails','id','Opportunities','opportunities','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Opportunities',0,0),
	('ddd6fc7b-e51e-bef9-210d-5ab8ff31a2bf','emails_tasks_rel','Emails','emails','id','Tasks','tasks','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Tasks',0,0),
	('de613e30-84e7-a293-454b-5ab8ff754239','emails_users_rel','Emails','emails','id','Users','users','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Users',0,0),
	('decc00b9-3c4b-be5b-98b8-5ab8ff72b143','emails_project_task_rel','Emails','emails','id','ProjectTask','project_task','id','emails_beans','email_id','bean_id','many-to-many','bean_module','ProjectTask',0,0),
	('df28dcf3-9c3b-4ae6-f2e3-5ab8fff094a2','aor_charts_modified_user','Users','users','id','AOR_Charts','aor_charts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('df93369f-fb34-e6de-c2f9-5ab8ff3262c2','aor_charts_created_by','Users','users','id','AOR_Charts','aor_charts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('e00a2486-9a8b-c0b6-7cbc-5ab8ff59c233','aor_charts_aor_reports','AOR_Reports','aor_reports','id','AOR_Charts','aor_charts','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('e0dc364f-fe2e-dd35-235c-5ab8ffc2a522','securitygroups_emailtemplates','SecurityGroups','securitygroups','id','EmailTemplates','email_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','EmailTemplates',0,0),
	('e3765175-a361-46dc-1a3f-5ab8ff901f69','aor_conditions_modified_user','Users','users','id','AOR_Conditions','aor_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('e67a4c1c-735b-5403-8971-5ab8ffb3c01c','aor_conditions_created_by','Users','users','id','AOR_Conditions','aor_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('e6d2a9ee-32c3-2b84-cdc7-5ab8ff0aee29','emails_projects_rel','Emails','emails','id','Project','project','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Project',0,0),
	('e74d743e-b97c-82a8-37d2-5ab8ffe7fcd6','emails_prospects_rel','Emails','emails','id','Prospects','prospects','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Prospects',0,0),
	('e837d52b-5d87-8171-fb38-5ab8ffd4aebe','meetings_contacts','Meetings','meetings','id','Contacts','contacts','id','meetings_contacts','meeting_id','contact_id','many-to-many',NULL,NULL,0,0),
	('e8b1fcad-bedc-7b17-0692-5ab8ff59354d','meetings_users','Meetings','meetings','id','Users','users','id','meetings_users','meeting_id','user_id','many-to-many',NULL,NULL,0,0),
	('e933dcb8-7bf7-79e2-9694-5ab8ffd48967','meetings_leads','Meetings','meetings','id','Leads','leads','id','meetings_leads','meeting_id','lead_id','many-to-many',NULL,NULL,0,0),
	('e95ac771-05d7-0657-4c93-5ab8ff8288c0','emailtemplates_assigned_user','Users','users','id','EmailTemplates','email_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('e9a27419-cf9e-6ea3-8dcc-5ab8ff1e2505','opportunities_contacts','Opportunities','opportunities','id','Contacts','contacts','id','opportunities_contacts','opportunity_id','contact_id','many-to-many',NULL,NULL,0,0),
	('ea1b454a-4a70-dde1-f4b1-5ab8fffcac42','prospect_list_campaigns','ProspectLists','prospect_lists','id','Campaigns','campaigns','id','prospect_list_campaigns','prospect_list_id','campaign_id','many-to-many',NULL,NULL,0,0),
	('eac2c3c1-9ff3-362a-0228-5ab8ff230558','prospect_list_contacts','ProspectLists','prospect_lists','id','Contacts','contacts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Contacts',0,0),
	('ebb1c4ec-5b28-749f-d1bb-5ab8ffffcbeb','prospect_list_prospects','ProspectLists','prospect_lists','id','Prospects','prospects','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Prospects',0,0),
	('ec006cf8-70b6-cb32-0f53-5ab8ffacdd3f','notes_assigned_user','Users','users','id','Notes','notes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ec2d4ab8-b0d3-2fb4-1358-5ab8ffd0a1d9','prospect_list_leads','ProspectLists','prospect_lists','id','Leads','leads','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Leads',0,0),
	('ec97971e-ee25-f639-198d-5ab8ffa5e551','securitygroups_notes','SecurityGroups','securitygroups','id','Notes','notes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Notes',0,0),
	('eca983aa-adc0-eeb6-d3fe-5ab8fff981b6','prospect_list_users','ProspectLists','prospect_lists','id','Users','users','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Users',0,0),
	('ed29d796-30d5-675a-964b-5ab8ff3f97f3','prospect_list_accounts','ProspectLists','prospect_lists','id','Accounts','accounts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Accounts',0,0),
	('ed59b261-bfd0-60ba-a09e-5ab8ff8bbfaa','notes_modified_user','Users','users','id','Notes','notes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ed832cad-5d38-d381-71f1-5ab8ff4ed2fd','roles_users','Roles','roles','id','Users','users','id','roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),
	('edcb9351-5e6f-1cb1-55a2-5ab8ff2bd883','notes_created_by','Users','users','id','Notes','notes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('ede6e73b-7d86-9dfb-fd97-5ab8ff847442','projects_bugs','Project','project','id','Bugs','bugs','id','projects_bugs','project_id','bug_id','many-to-many',NULL,NULL,0,0),
	('ee732d77-cf28-16f9-b9a1-5ab8ff28c285','projects_cases','Project','project','id','Cases','cases','id','projects_cases','project_id','case_id','many-to-many',NULL,NULL,0,0),
	('eeec3394-2314-d234-c744-5ab8ff8cca8f','projects_accounts','Project','project','id','Accounts','accounts','id','projects_accounts','project_id','account_id','many-to-many',NULL,NULL,0,0),
	('ef66c740-d648-541f-53aa-5ab8ffbe39af','projects_contacts','Project','project','id','Contacts','contacts','id','projects_contacts','project_id','contact_id','many-to-many',NULL,NULL,0,0),
	('efe743cb-48a3-9c27-1b19-5ab8ff587720','projects_opportunities','Project','project','id','Opportunities','opportunities','id','projects_opportunities','project_id','opportunity_id','many-to-many',NULL,NULL,0,0),
	('f07acbbc-231c-ce90-a0f3-5ab8ff32c2d8','acl_roles_actions','ACLRoles','acl_roles','id','ACLActions','acl_actions','id','acl_roles_actions','role_id','action_id','many-to-many',NULL,NULL,0,0),
	('f10aba02-ecf8-7e38-3edd-5ab8ff2c986b','acl_roles_users','ACLRoles','acl_roles','id','Users','users','id','acl_roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),
	('f175f3c0-b9ef-3f3c-9a36-5ab8ff4be3a3','email_marketing_prospect_lists','EmailMarketing','email_marketing','id','ProspectLists','prospect_lists','id','email_marketing_prospect_lists','email_marketing_id','prospect_list_id','many-to-many',NULL,NULL,0,0),
	('f184ef7c-cecd-9752-7fab-5ab8ffeef65a','calls_modified_user','Users','users','id','Calls','calls','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('f1f87997-f720-ec47-e844-5ab8ff7c4af9','leads_documents','Leads','leads','id','Documents','documents','id','linked_documents','parent_id','document_id','many-to-many','parent_type','Leads',0,0),
	('f227a4a5-747d-99ed-31e7-5ab8ffd20437','calls_created_by','Users','users','id','Calls','calls','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('f27c2a5f-e9b8-b117-b072-5ab8ffbe6b0f','documents_accounts','Documents','documents','id','Accounts','accounts','id','documents_accounts','document_id','account_id','many-to-many',NULL,NULL,0,0),
	('f2b058e1-66ce-4a45-61f2-5ab8ff1f1b3a','calls_assigned_user','Users','users','id','Calls','calls','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),
	('f32236d8-bf5e-a0e7-3890-5ab8ff1124e7','documents_contacts','Documents','documents','id','Contacts','contacts','id','documents_contacts','document_id','contact_id','many-to-many',NULL,NULL,0,0),
	('f3a3b096-90fa-00f2-1447-5ab8ff0d8049','documents_opportunities','Documents','documents','id','Opportunities','opportunities','id','documents_opportunities','document_id','opportunity_id','many-to-many',NULL,NULL,0,0),
	('f3b22e75-4838-64da-a519-5ab8ff80218f','securitygroups_calls','SecurityGroups','securitygroups','id','Calls','calls','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Calls',0,0);

/*!40000 ALTER TABLE `relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы releases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `releases`;

CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы reminders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `popup` tinyint(1) DEFAULT NULL,
  `email` tinyint(1) DEFAULT NULL,
  `email_sent` tinyint(1) DEFAULT NULL,
  `timer_popup` varchar(32) DEFAULT NULL,
  `timer_email` varchar(32) DEFAULT NULL,
  `related_event_module` varchar(32) DEFAULT NULL,
  `related_event_module_id` char(36) NOT NULL,
  `date_willexecute` int(60) DEFAULT '-1',
  `popup_viewed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_reminder_name` (`name`),
  KEY `idx_reminder_deleted` (`deleted`),
  KEY `idx_reminder_related_event_module` (`related_event_module`),
  KEY `idx_reminder_related_event_module_id` (`related_event_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы reminders_invitees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reminders_invitees`;

CREATE TABLE `reminders_invitees` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reminder_id` char(36) NOT NULL,
  `related_invitee_module` varchar(32) DEFAULT NULL,
  `related_invitee_module_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reminder_invitee_name` (`name`),
  KEY `idx_reminder_invitee_assigned_user_id` (`assigned_user_id`),
  KEY `idx_reminder_invitee_reminder_id` (`reminder_id`),
  KEY `idx_reminder_invitee_related_invitee_module` (`related_invitee_module`),
  KEY `idx_reminder_invitee_related_invitee_module_id` (`related_invitee_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы roles_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_modules`;

CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы saved_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `saved_search`;

CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы schedulers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schedulers`;

CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schedulers` WRITE;
/*!40000 ALTER TABLE `schedulers` DISABLE KEYS */;

INSERT INTO `schedulers` (`id`, `deleted`, `date_entered`, `date_modified`, `created_by`, `modified_user_id`, `name`, `job`, `date_time_start`, `date_time_end`, `job_interval`, `time_from`, `time_to`, `last_run`, `status`, `catch_up`)
VALUES
	('13ec914b-d77b-1ad0-5f3d-5ab8f724829f',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Perform Lucene Index','function::aodIndexUnindexed','2015-01-01 14:00:01',NULL,'0::0::*::*::*',NULL,NULL,NULL,'Active',0),
	('1e3a76ee-f97c-e92d-3f61-5ab8f7f4e4b7',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Optimise AOD Index','function::aodOptimiseIndex','2015-01-01 14:00:01',NULL,'0::*/3::*::*::*',NULL,NULL,NULL,'Active',0),
	('2b8fa344-9ce2-06b0-030e-5ab8f73d27a6',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Run Email Reminder Notifications','function::sendEmailReminders','2015-01-01 06:45:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),
	('332da04e-7168-1a20-c12b-5ab8f7ad06ba',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Clean Jobs Queue','function::cleanJobQueue','2015-01-01 11:15:01',NULL,'0::5::*::*::*',NULL,NULL,NULL,'Active',0),
	('3af67573-8362-eb65-44e6-5ab8f7773cdd',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Removal of documents from filesystem','function::removeDocumentsFromFS','2015-01-01 10:45:01',NULL,'0::3::1::*::*',NULL,NULL,NULL,'Active',0),
	('3fff1f68-0b29-5529-97f1-5ab8f7057100',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Prune SuiteCRM Feed Tables','function::trimSugarFeeds','2015-01-01 17:45:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),
	('88ce0dca-2bf8-c0e3-bdb9-5ab8f7e0195d',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Run Nightly Mass Email Campaigns','function::runMassEmailCampaign','2015-01-01 13:30:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),
	('b75c8c91-305c-2e5e-84db-5ab8f7f971b8',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Process Workflow Tasks','function::processAOW_Workflow','2015-01-01 12:30:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),
	('bdcc50e3-dcd9-81c2-17e3-5ab8f73f0862',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Run Report Generation Scheduled Tasks','function::aorRunScheduledReports','2015-01-01 12:45:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),
	('c9a11da2-3d90-d7cb-a1fe-5ab8f7403c8c',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Prune Tracker Tables','function::trimTracker','2015-01-01 10:15:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),
	('d4d7886d-ac67-0579-604d-5ab8f7eb8983',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Check Inbound Mailboxes','function::pollMonitoredInboxesAOP','2015-01-01 15:15:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),
	('e2600e00-9021-6368-af2c-5ab8f7aea26b',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Prune Database on 1st of Month','function::pruneDatabase','2015-01-01 06:00:01',NULL,'0::4::1::*::*',NULL,NULL,NULL,'Inactive',0),
	('effc3312-546f-6080-8f0b-5ab8f74670a6',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','1','Run Nightly Process Bounced Campaign Emails','function::pollMonitoredInboxesForBouncedCampaignEmails','2015-01-01 08:45:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1);

/*!40000 ALTER TABLE `schedulers` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы securitygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups`;

CREATE TABLE `securitygroups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы securitygroups_acl_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups_acl_roles`;

CREATE TABLE `securitygroups_acl_roles` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `role_id` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы securitygroups_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups_audit`;

CREATE TABLE `securitygroups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы securitygroups_default
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups_default`;

CREATE TABLE `securitygroups_default` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы securitygroups_records
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups_records`;

CREATE TABLE `securitygroups_records` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `record_id` char(36) DEFAULT NULL,
  `module` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_records_mod` (`module`,`deleted`,`record_id`,`securitygroup_id`),
  KEY `idx_securitygroups_records_del` (`deleted`,`record_id`,`module`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы securitygroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `securitygroups_users`;

CREATE TABLE `securitygroups_users` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `securitygroup_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `primary_group` tinyint(1) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `securitygroups_users_idxa` (`securitygroup_id`),
  KEY `securitygroups_users_idxb` (`user_id`),
  KEY `securitygroups_users_idxc` (`user_id`,`deleted`,`securitygroup_id`,`id`),
  KEY `securitygroups_users_idxd` (`user_id`,`deleted`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы spots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `spots`;

CREATE TABLE `spots` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `config` longtext,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы sugarfeed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sugarfeed`;

CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestionoptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestionoptions`;

CREATE TABLE `surveyquestionoptions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `sort_order` int(255) DEFAULT NULL,
  `survey_question_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestionoptions_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestionoptions_audit`;

CREATE TABLE `surveyquestionoptions_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestionoptions_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestionoptions_surveyquestionresponses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestionoptions_surveyquestionresponses`;

CREATE TABLE `surveyquestionoptions_surveyquestionresponses` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `surveyq72c7options_ida` varchar(36) DEFAULT NULL,
  `surveyq10d4sponses_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyquestionoptions_surveyquestionresponses_alt` (`surveyq72c7options_ida`,`surveyq10d4sponses_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestionresponses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestionresponses`;

CREATE TABLE `surveyquestionresponses` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `answer` text,
  `answer_bool` tinyint(1) DEFAULT NULL,
  `answer_datetime` datetime DEFAULT NULL,
  `surveyquestion_id` char(36) DEFAULT NULL,
  `surveyresponse_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestionresponses_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestionresponses_audit`;

CREATE TABLE `surveyquestionresponses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestionresponses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestions`;

CREATE TABLE `surveyquestions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `sort_order` int(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `happiness_question` tinyint(1) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyquestions_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyquestions_audit`;

CREATE TABLE `surveyquestions_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestions_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyresponses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyresponses`;

CREATE TABLE `surveyresponses` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `happiness` int(11) DEFAULT NULL,
  `email_response_sent` tinyint(1) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveyresponses_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveyresponses_audit`;

CREATE TABLE `surveyresponses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyresponses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveys
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveys`;

CREATE TABLE `surveys` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `submit_text` varchar(255) DEFAULT 'Submit',
  `satisfied_text` varchar(255) DEFAULT 'Satisfied',
  `neither_text` varchar(255) DEFAULT 'Neither Satisfied nor Dissatisfied',
  `dissatisfied_text` varchar(255) DEFAULT 'Dissatisfied',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы surveys_audit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `surveys_audit`;

CREATE TABLE `surveys_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveys_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`),
  KEY `idx_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы templatesectionline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `templatesectionline`;

CREATE TABLE `templatesectionline` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `grp` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы tracker
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tracker`;

CREATE TABLE `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`),
  KEY `idx_tracker_date_modified` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы upgrade_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `upgrade_history`;

CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `upgrade_history` WRITE;
/*!40000 ALTER TABLE `upgrade_history` DISABLE KEYS */;

INSERT INTO `upgrade_history` (`id`, `filename`, `md5sum`, `type`, `status`, `version`, `name`, `description`, `id_name`, `manifest`, `date_entered`, `enabled`)
VALUES
	('76875e73-d74b-db22-3ca9-5ab8f7f2047e','upload/upgrades/langpack/ru.zip','ecb55431bceade4ef47b31007d4f76b7','langpack','installed','7.10.1.2','','','','YTozOntzOjg6Im1hbmlmZXN0IjthOjk6e3M6NDoibmFtZSI7czoxNjoiUnVzc2lhbiAoUnVzc2lhKSI7czoxMToiZGVzY3JpcHRpb24iO3M6NjA6ItCf0LXRgNC10LLQvtC0OiB3d3cuY3Jvd2Rpbi5jb20vcHJvamVjdC9zdWl0ZWNybXRyYW5zbGF0aW9ucyI7czo0OiJ0eXBlIjtzOjg6ImxhbmdwYWNrIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtzOjM6IlllcyI7czoyNToiYWNjZXB0YWJsZV9zdWdhcl92ZXJzaW9ucyI7YTowOnt9czoyNDoiYWNjZXB0YWJsZV9zdWdhcl9mbGF2b3JzIjthOjE6e2k6MDtzOjI6IkNFIjt9czo2OiJhdXRob3IiO3M6MTg6IlN1aXRlQ1JNIENvbW11bml0eSI7czo3OiJ2ZXJzaW9uIjtzOjg6IjcuMTAuMS4yIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxMDoiMjAxOC0wMy0xMyI7fXM6MTE6Imluc3RhbGxkZWZzIjthOjM6e3M6MjoiaWQiO3M6NToicnVfUlUiO3M6OToiaW1hZ2VfZGlyIjtzOjE3OiI8YmFzZXBhdGg+L2ltYWdlcyI7czo0OiJjb3B5IjthOjM6e2k6MDthOjI6e3M6NDoiZnJvbSI7czoxODoiPGJhc2VwYXRoPi9pbmNsdWRlIjtzOjI6InRvIjtzOjc6ImluY2x1ZGUiO31pOjE7YToyOntzOjQ6ImZyb20iO3M6MTg6IjxiYXNlcGF0aD4vbW9kdWxlcyI7czoyOiJ0byI7czo3OiJtb2R1bGVzIjt9aToyO2E6Mjp7czo0OiJmcm9tIjtzOjE4OiI8YmFzZXBhdGg+L2luc3RhbGwiO3M6MjoidG8iO3M6NzoiaW5zdGFsbCI7fX19czoxNjoidXBncmFkZV9tYW5pZmVzdCI7czowOiIiO30=','2018-03-26 13:37:11',1);

/*!40000 ALTER TABLE `upgrade_history` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы user_preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_preferences`;

CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;

INSERT INTO `user_preferences` (`id`, `category`, `deleted`, `date_entered`, `date_modified`, `assigned_user_id`, `contents`)
VALUES
	('1014efd4-e5c8-9db5-cf8b-5ab8f7e16c69','Home',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjkxYTgzNjVhLTIxNjUtZTIzYy0wMmRlLTVhYjhmNzllM2NhZCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjkyNWM0YjhiLTllODMtNWI3MS1lMWE3LTVhYjhmN2I2NzE0OCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjkzNjllMGM0LTBiNDYtYjViYS03M2RiLTVhYjhmNzdiYmM1MyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6Ijk0NjA0YzM2LTM2MDYtOGExMy03Y2U4LTVhYjhmNzA2MWM2YiI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiOTVhNjk1YWUtMDM1NC0xYjI1LTk3MTktNWFiOGY3Y2VhNTY1IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiOTZjZWY0MDQtZjhhMC05MTA2LTVlYjgtNWFiOGY3ZmVhODkxIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiI5MjVjNGI4Yi05ZTgzLTViNzEtZTFhNy01YWI4ZjdiNjcxNDgiO2k6MTtzOjM2OiI5MzY5ZTBjNC0wYjQ2LWI1YmEtNzNkYi01YWI4Zjc3YmJjNTMiO2k6MjtzOjM2OiI5NDYwNGMzNi0zNjA2LThhMTMtN2NlOC01YWI4ZjcwNjFjNmIiO2k6MztzOjM2OiI5NWE2OTVhZS0wMzU0LTFiMjUtOTcxOS01YWI4ZjdjZWE1NjUiO2k6NDtzOjM2OiI5NmNlZjQwNC1mOGEwLTkxMDYtNWViOC01YWI4ZjdmZWE4OTEiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjkxYTgzNjVhLTIxNjUtZTIzYy0wMmRlLTVhYjhmNzllM2NhZCI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),
	('1395fc3d-a977-15b3-e746-5ab8f726d039','Home2_CALL',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('16a097e5-b801-931f-fd42-5ab8f74289ee','Home2_MEETING',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('1971ae9f-c06f-e228-1835-5ab8f7b13798','Home2_LEAD_96cef404-f8a0-9106-5eb8-5ab8f7fea891',0,'2018-03-26 13:35:22','2018-03-26 13:35:22','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('1e56afb2-bef6-e946-3f2c-5ab8f7fe65fb','Home2_OPPORTUNITY',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('2443a668-3456-7efd-4c63-5ab8f7f990d3','Home2_ACCOUNT',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('2b300e52-7bed-c71f-6275-5ab8f702c5d4','Home2_LEAD',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('31308bf9-fe75-cb60-bdee-5ab8f77177ef','Home2_SUGARFEED',0,'2018-03-26 13:35:19','2018-03-26 13:35:19','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
	('bb4b4911-2aac-2217-f834-5ab8f7ededa8','global',0,'2018-03-26 13:34:43','2018-03-26 13:38:28','1','YToyOTp7czo4OiJ0aW1lem9uZSI7czoxMzoiRXVyb3BlL0JlcmxpbiI7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWl0ZVAiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjEzOiJyZW1pbmRlcl90aW1lIjtpOjE4MDA7czoxOToiZW1haWxfcmVtaW5kZXJfdGltZSI7aTozNjAwO3M6MTY6InJlbWluZGVyX2NoZWNrZWQiO3M6MToiMSI7czoyOiJ1dCI7czoxOiIxIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjE1OiJtYWlsX3NtdHBzZXJ2ZXIiO3M6MDoiIjtzOjEzOiJtYWlsX3NtdHBwb3J0IjtzOjI6IjI1IjtzOjEzOiJtYWlsX3NtdHB1c2VyIjtzOjA6IiI7czoxMzoibWFpbF9zbXRwcGFzcyI7czowOiIiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czoxOiIxIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MTtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6MjE6ImRlZmF1bHRfZW1haWxfY2hhcnNldCI7czo1OiJVVEYtOCI7czoxOToidGhlbWVfY3VycmVudF9ncm91cCI7czo2OiLQktGB0LUiO30='),
	('bfa5025d-de11-a816-0cfe-5ab8f7d2c808','ETag',0,'2018-03-26 13:34:43','2018-03-26 13:34:43','1','YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6MTt9');

/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL,
  `factor_auth` tinyint(1) DEFAULT NULL,
  `factor_auth_interface` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `user_name`, `user_hash`, `system_generated_password`, `pwd_last_changed`, `authenticate_id`, `sugar_login`, `first_name`, `last_name`, `is_admin`, `external_auth_only`, `receive_notifications`, `description`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `title`, `photo`, `department`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `status`, `address_street`, `address_city`, `address_state`, `address_country`, `address_postalcode`, `deleted`, `portal_only`, `show_on_employees`, `employee_status`, `messenger_id`, `messenger_type`, `reports_to_id`, `is_group`, `factor_auth`, `factor_auth_interface`)
VALUES
	('1','admin','$1$ysnA9m40$rY8h9CvuU4zIyXirjKh6C0',0,'2018-03-26 13:35:18',NULL,1,NULL,'Administrator',1,0,1,NULL,'2018-03-26 13:34:43','2018-03-26 13:35:18','1','','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'',0,0,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы users_feeds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_feeds`;

CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы users_last_import
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_last_import`;

CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы users_password_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_password_link`;

CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы users_signatures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_signatures`;

CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы vcals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vcals`;

CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
