<?php
$mod_strings['LBL_LOGIN_DEMO_USERS'] = 'Демонстрационные учетные записи';
$mod_strings['LBL_LOGIN_DEMO_LOGPASS'] = 'или вход по логину и паролю';
$mod_strings['LBL_SUITE_CRMHOSTING'] = 'CRMHosting.ru';
$mod_strings['LBL_CRMHOSTING_DESC1'] = '<A href="https://crmhosting.ru/">CRMHosting.ru</A> - разработка и адаптация CRM-систем на базе SugarCRM и SuiteCRM.';
