<?php
if (!defined('sugarEntry')) define('sugarEntry', true);

require_once('include/entryPoint.php');
global $db;

if (empty($current_language)) {
    $current_language = $sugar_config['default_language'];
}

$app_list_strings = return_app_list_strings_language($current_language);
$app_strings = return_application_language($current_language);

global $current_user;
$current_user = new User();
$current_user->getSystemUser();


require_once('modules/Administration/QuickRepairAndRebuild.php');

$randc = new RepairAndClear();
$randc->repairAndClearAll(array('clearAll'), array(translate('LBL_ALL_MODULES')), false, true);
